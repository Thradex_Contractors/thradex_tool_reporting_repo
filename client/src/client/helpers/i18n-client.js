import i18next from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import path from 'path';
import FilesystemBackend from './i18n-fs-backend';

export const initTrans = renderCallback => {
  window.i18next = i18next;
  i18next.t = i18next.t.bind(i18next);
  i18next
    .use(LanguageDetector)
    .use(FilesystemBackend)
    .init({
      ns: [
        'trans',
      ],
      defaultNS: 'trans',
      whitelist: [
        // 'en', // Uncomment if we want to support english translations
        'es',
      ],
      preload: [
        'es',
      ],
      fallbackLng: 'es',
      backend: {
        loadPath: path.resolve(__dirname, '../locales/{{lng}}/{{ns}}.yml'),
      },
      detection: {
        order: ['navigator', 'querystring', 'cookie', 'localStorage'],
        lookupQuerystring: 'lng',
        lookupCookie: 'i18next',
        lookupLocalStorage: 'i18nextLng',
        caches: ['localStorage', 'cookie'],
      },
    }, renderCallback);
};

