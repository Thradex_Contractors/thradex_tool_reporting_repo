import YAML from 'js-yaml';

const getDefaults = () => ({
  loadPath: '/locales/{{lng}}/{{ns}}.json',
});

function readFile(filename, callback) {
  const httpRequest = new XMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState === 4) {
      if (httpRequest.status === 200) {
        const data = httpRequest.responseText;
        let result;
        try {
          result = YAML.safeLoad(data);
        } catch (err) {
          err.message = `error parsing ${filename}: ${err.message}`;
          callback(err);
          return;
        }

        callback(null, result);
      } else {
        callback(httpRequest.statusText);
      }
    }
  };

  httpRequest.open('GET', filename);
  httpRequest.send();
}

class FilesystemBackend {
  constructor(services, options = {}) {
    this.init(services, options);
    this.type = 'backend';
  }

  init(services, options) {
    this.services = services;
    this.options = options || getDefaults();
  }

  read(language, namespace, callback) {
    const filename = this.services.interpolator.interpolate(this.options.loadPath, { lng: language, ns: namespace });
    readFile(filename, (err, resources) => {
      if (err) {
        callback(err, false);
        return;
      }

      callback(null, resources);
    });
  }
}

FilesystemBackend.type = 'backend';

export default FilesystemBackend;
