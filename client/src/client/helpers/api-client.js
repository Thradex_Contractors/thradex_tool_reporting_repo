import superagent from 'superagent';
import URL from 'url';
import dispatcher from 'dispatchy';
import merge from 'lodash/merge';
import config from '../config';

export default class ApiClient {
  constructor() {
    this.counter = 0;
    this.uploadRequestMap = new Map();
    merge(this, dispatcher.create());

    ['get', 'post', 'put', 'patch', 'del'].forEach(method => {
      this[method] = (path, options) => {
        this.counter++;
        const args = {
          id: `${Date.now()}_${this.counter}`,
          resource: `${method}_${path}`,
          method,
          path,
        };

        this.fire('request:start', args);
        const methodPromise = new Promise((resolve, reject) => {
          const url = this.formatUrl(path);
          const request = this.getRequestOptions(url, method, options);

          request.end((err, { body } = { body: undefined }) => {
            if (err) {
              reject(body || err);
            } else {
              resolve(body);
            }
          });
        });

        methodPromise.then(() => this.fire('request:end', args), () => this.fire('request:end', { ...args, error: true }));
        return methodPromise;
      };
    });
  }

  upload(path, formData, settings) {
    this.counter++;

    const args = {
      id: (settings && settings.requestId) || `${Date.now()}_${this.counter}`,
      resource: `post_${path}`,
      method: 'post',
      path,
    };

    this.fire('request:start', args);
    const uploadPromise = new Promise((resolve, reject) => {
      const request = superagent.post(this.formatUrl(path)).send(formData);

      this.uploadRequestMap.set(args.id, request);
      if (this.headers) {
        request.set(this.headers);
      }

      if (settings && settings.reportProgress) {
        request.on('progress', e => {
          this.fire('request:progress', { ...args, percent: e.percent });
        });
      }

      request.end((err, res) => {
        this.uploadRequestMap.delete(args.id);
        if (err) {
          reject((res && res.body) || err);
        } else {
          resolve(res.body);
        }
      });
    });

    uploadPromise.then(() => this.fire('request:end', args)).catch(error => this.fire('request:end', { ...args, error }));

    return uploadPromise;
  }

  abortUpload(requestId) {
    if (this.uploadRequestMap.has(requestId)) {
      this.uploadRequestMap.get(requestId).abort();
      this.uploadRequestMap.delete(requestId);
    }
  }

  getRequestOptions(url, method, options = {}) {
    const request = superagent[method](url);

    if (options && options.params) {
      request.query(options.params);
    }

    if (this.headers) {
      request.set(this.headers);
    }

    if (options && options.headers) {
      request.set(options.headers);
    }

    if (options && options.data) {
      request.send(options.data);
    }

    return request;
  }

  formatUrl(path) {
    const url = path[0] === '/' ? path.substring(1) : path;
    return URL.resolve(config.apiUrl, url);
  }

  clearHeaders() {
    this.headers = null;
  }

  setExtraHeaders(headers) {
    this.headers = {
      ...headers,
    };
  }
}
