const counters = {};

export default function generateId(instance) {
  const name = instance.name || instance.displayName || instance.constructor.name || 'unnamed';

  const counterForName = (counters[name] = counters[name] || 0);
  counters[name]++;

  return `${name}_${counterForName}`;
}
