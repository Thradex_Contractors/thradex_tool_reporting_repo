export const IS_WORD_FILE = /^docx?$/i;
export const IS_PDF_FILE = /^pdf$/i;
export const IS_IMAGE_FILE = /^png$|^gif$|^jpe?g$/i;
