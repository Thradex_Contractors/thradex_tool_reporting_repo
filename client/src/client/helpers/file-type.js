export const DOCTypes = {
  doc: 'doc',
  docx: 'docx',
  xls: 'xls',
  xlsx: 'xlsx',
  pdf: 'pdf',
  png: 'png',
  gif: 'gif',
  jpeg: 'jpeg',
};

const MIMETypesHash = {
  [DOCTypes.doc]: 'application/msword',
  [DOCTypes.docx]: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  [DOCTypes.xls]: 'application/vnd.ms-excel',
  [DOCTypes.xlsx]: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  [DOCTypes.pdf]: 'application/pdf',
  [DOCTypes.png]: 'image/png',
  [DOCTypes.gif]: 'image/gif',
  [DOCTypes.jpeg]: 'image/jpeg',
};

export const getSupportedMIMETypes = (docTypes = []) =>
  Object.keys(MIMETypesHash)
    .filter(key => docTypes.some(type => type === key))
    .map(key => MIMETypesHash[key]);
