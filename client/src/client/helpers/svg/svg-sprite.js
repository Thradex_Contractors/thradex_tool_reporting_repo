import cheerio from 'cheerio';
import path from 'path';
import globModule from 'glob';
import fse from 'fs-extra';
import thenify from '../thenify';

const mkdirp = thenify(fse.ensureDir);
const readFile = thenify(fse.readFile);
const glob = thenify(globModule);
const writeFile = thenify(fse.writeFile);

const write = async (file, contents, options) => {
  const dir = path.dirname(file);
  await mkdirp(dir);
  return await writeFile(file, contents, options);
};

const generate = async (overrideOriginal = false) => {
  const files = await glob('./src/client/assets/icons/**/*.svg');

  const svgs = await files.reduce(async (seq, file) => {
    const key = path.basename(file, '.svg');
    const text = await readFile(file, { encoding: 'utf8' });

    const $ = cheerio.load(text);
    const res = await seq;

    const p = $('path');

    p.removeAttr('sketch:type');
    p.removeAttr('stroke');
    res[key] = p.toString();

    if (overrideOriginal) {
      const template = `
        <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
          <g>
            ${res[key]}
          </g>
        </svg>`.trim().replace(/\n\s{8}/g, '\n');

      const fileName = `./src/client/assets/icons/${key}.svg`;

      await write(fileName, template);
    }

    return res;
  }, Promise.resolve({}));

  const resourceFile = './src/client/helpers/svg/sprite.js';
  await write(resourceFile, `module.exports = ${JSON.stringify(svgs, null, 2)}`);
};

// Usage:
// node_modules/.bin/babel-node src/client/helpers/svg/svg-sprite.js
generate(process.argv.indexOf('--override-original') > -1)
  .catch(err => console.log('[svg-sprite]', err));
