import React from 'react';
import { createDevTools } from 'redux-devtools';
import LogMonitor from 'redux-devtools-log-monitor';
import DockMonitor from 'redux-devtools-dock-monitor';

export const DevTools = createDevTools( // eslint-disable-line
  <DockMonitor toggleVisibilityKey="ctrl-H"
               changePositionKey="ctrl-Q"
               defaultIsVisible={ false }>
    <LogMonitor />
  </DockMonitor>);
