export const toTitleCase = (str = '') =>
  str
    .split(' ')
    .map((word, idx) => {
      if (!word) return word;
      const shouldCap = !idx || word.length > 3;
      const firstLetter = shouldCap ? word[0].toUpperCase() : word[0].toLowerCase();
      return firstLetter + word.substring(1);
    })
    .join(' ');

export const toSentenceCase = (str = '') =>
  str
    .split(' ')
    .map((word, idx) => {
      if (!word) return word;

      // QUESTION: this assumes children are all strings.  Is that valid?
      if (str.children) return str.children.map(child => toSentenceCase(child));

      const shouldCap = !idx;
      const firstLetter = shouldCap ? word[0].toUpperCase() : word[0].toLowerCase();
      return firstLetter + word.substring(1);
    })
    .join(' ');
