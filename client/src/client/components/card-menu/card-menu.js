import React, { Component, PropTypes } from 'react';
import IconButton from '../icon-button/icon-button';
import Menu from '../menu/menu';

import generateId from '../../helpers/generateId';

export default class CardMenu extends Component {
  static propTypes = {
    menuListClassName: PropTypes.string,
    iconClassName: PropTypes.string,
    iconName: PropTypes.string,
    iconStyle: PropTypes.string,
    trigger: PropTypes.object,
    expandTo: PropTypes.string,
    positionArgs: PropTypes.object,
    buttonStyle: PropTypes.object,
    noAutoBind: PropTypes.bool,
    onSelect: PropTypes.func,
  };

  static defaultProps = {
    noAutoBind: true,
    expandTo: 'bottom-left',
    positionArgs: {
      my: 'right top',
      at: 'right top',
    },
  };

  constructor(props) {
    super(props);
    this.id = generateId(this);
    this.state = {};
  }

  open() {
    if (this.state.open) {
      return;
    }
    this.setState({ open: true });
  }

  close() {
    if (!this.state.open) {
      return;
    }
    this.setState({ open: false });
  }

  toggle() {
    if (this.state.open) {
      this.close();
    } else {
      this.open();
    }
  }

  render() {
    const {
      children,
      expandTo,
      positionArgs,
      buttonStyle,
      trigger,
      iconStyle,
      iconName,
      noAutoBind,
      iconClassName,
      menuListClassName,
      onSelect,
      menuListStyle,
    } = this.props;

    let triggerC = trigger;

    if (!triggerC) {
      triggerC = (
        <IconButton
          data-component="card-menu"
          onClick={ () => this.setState({ open: true }) }
          iconName={ iconName }
          className={ iconClassName }
          iconStyle={ iconStyle }
          style={ buttonStyle }
        />
      );
    }

    const { open } = this.state;

    return (
      <Menu
        open={ open }
        expandTo={ expandTo }
        noAutoBind={ noAutoBind }
        positionArgs={ positionArgs }
        menuListClassName={ menuListClassName }
        menuListStyle={ menuListStyle }
        onSelect={ onSelect }
        onCloseRequest={ () => this.setState({ open: false }) }
        trigger={ triggerC }>
        {children}
      </Menu>
    );
  }
}
