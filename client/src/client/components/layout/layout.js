import React, { Component } from 'react';

const createElement = nameOfElement => {
  const layout = class Adapter extends Component {
    render() {
      const { children, className, ...restProperties } = this.props;
      return <div className={ className } { ...restProperties }>{ children }</div>;
    }
  };
  layout.displayName = nameOfElement;
  return layout;
};

const Layout = createElement('layout');
Layout.Header = createElement('header');
Layout.Footer = createElement('footer');
Layout.Content = createElement('content');

export default Layout;
