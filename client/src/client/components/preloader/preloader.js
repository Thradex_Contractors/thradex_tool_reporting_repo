import React from 'react';
import { cf } from './preloader.scss';

export default ({ size = 'normal' }) => (
  <div
    className={ cf('preloader-wrapper active', {
      big: size === 'big',
      small: size === 'small',
    }) }>
    <div className={ cf('spinner-layer') }>
      <div className={ cf('circle-clipper left') }>
        <div className={ cf('circle') } />
      </div>
      <div className={ cf('gap-patch') }>
        <div className={ cf('circle') } />
      </div>
      <div className={ cf('circle-clipper right') }>
        <div className={ cf('circle') } />
      </div>
    </div>
  </div>
);
