import React, { Component, PropTypes } from 'react';
import { observer } from 'mobx-react';
import Dropzone from 'react-dropzone';
import { t } from 'i18next';
import Text from '../typography/text';
import Title from '../typography/title';
import Caption from '../typography/caption';
import Icon from '../icon/icon';
import Card from '../card/card';
import Button from '../button/button';
import { cf, g } from './multi-file-uploader.scss';
import generateId from '../../helpers/generateId';
import { DOCTypes, getSupportedMIMETypes } from '../../helpers/file-type';
import { client } from './rest-client';
import { FileQueue } from './file-queue';
import { FileQueueModel } from './file-queue-model';
import { uploadState } from './upload-state';

@observer
export default class MultiFileUploader extends Component {
  static propTypes = {
    supportedFileFormats: PropTypes.array,
    multiple: PropTypes.bool,
    disabled: PropTypes.bool,
    fileSize: PropTypes.number,
    token: PropTypes.string,
    uploadPath: PropTypes.string.isRequired,
    sharedFiles: PropTypes.bool,
    queueType: PropTypes.string,
    partyApplicationId: PropTypes.string,
    files: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      type: PropTypes.string,
      size: PropTypes.number,
      metadata: PropTypes.shape({
        categoryId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      }),
    })),
    onFileUploaded: PropTypes.func,
    onCancelUpload: PropTypes.func,
    onDeleteItem: PropTypes.func,
    context: PropTypes.string,
  };

  static defaultProps = {
    files: [],
    multiple: true,
    fileSize: 20, // in megabytes - remember there is a server side validations too.
    supportedFileFormats: Object.keys(DOCTypes),
  };

  constructor(props, context) {
    super(props, context);

    if (props.token) {
      client.setExtraHeaders({
        Authorization: `Bearer ${props.token}`,
      });
    }

    this.state = {
      model: this.createModel(props.files),
      supportedMIMETypes: new Set(getSupportedMIMETypes(this.props.supportedFileFormats)),
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.supportedFileFormats !== nextProps.supportedFileFormats) {
      this.setState({
        supportedMIMETypes: new Set(getSupportedMIMETypes(nextProps.supportedFileFormats)),
      });
    }
  }

  get isDirty() {
    const { model } = this.state || {};
    return model.isDirty();
  }

  get errors() {
    const { model } = this.state || {};
    return model.errors;
  }

  get queueType() {
    return this.props.queueType;
  }

  createModel(files) {
    const validations = {
      type: {
        validator: this.isValidFileType,
        message: t('INVALID_FILE_TYPE'),
      },
      size: {
        validator: this.isValidFileSize,
        message: t('LIMIT_FILE_SIZE', { fileSize: this.props.fileSize }),
      },
    };

    const metadata = {};
    if (this.props.sharedFiles) {
      metadata.partyApplicationId = this.props.partyApplicationId;
    }
    return new FileQueueModel({
      uploadState,
      validations,
      files,
      apiClient: client,
      uploadPath: this.props.uploadPath,
      serverErrorMessages: {
        size: t('LIMIT_FILE_SIZE', { fileSize: this.props.fileSize }),
        generic: t('SERVER_ERROR'),
      },
      context: this.props.context,
      metadata,
    });
  }

  isValidFileType = type => this.state.supportedMIMETypes.has(type);

  isValidFileSize = size => {
    const fileSizeInMegabytes = size / 1000000.0;
    return fileSizeInMegabytes <= this.props.fileSize;
  };

  handleDrop = files => {
    const { model } = this.state || {};

    files.forEach(file => {
      file.clientId = generateId(this);
      model.add(file);
    });

    const { queueType, onFileUploaded } = this.props;
    model.upload(uploadResponse => {
      const fileEntry = model.getFileById(uploadResponse.id);
      fileEntry && onFileUploaded && onFileUploaded(queueType, fileEntry);
    });
  };

  handleCancelUploadingFile = file => {
    const { queueType, onCancelUpload } = this.props;
    this.state.model.cancelUploadingFile(file.clientId);
    onCancelUpload && onCancelUpload(queueType, file);
  };

  handleDeleteUploadedFile = async file => {
    const { queueType, onDeleteItem } = this.props;
    await this.state.model.deleteUploadedFile(file.id);
    onDeleteItem && onDeleteItem(queueType, file);
  };

  getSupportedFilesStr = () => this.props.supportedFileFormats.map(format => format.toUpperCase()).join(', ');

  renderPlaceHolder = () => (
    <div className={ cf('placeholder') }>
      <Button type="flat" btnRole="primary" className={ cf('title') }>
        <Icon name="upload" />
        <span>{t('PLACEHOLDER_CHOOSE_FILE_TO_UPLOAD')}</span>
      </Button>
      <Text disabled className={ cf('option') }>
        {t('PLACEHOLDER_OPTION_TO_UPLOAD')}
      </Text>
      <Title secondary>{t('PLACEHOLDER_DRAG_FILES_HERE')}</Title>
    </div>
  );

  render({ className, multiple, disabled } = this.props) {
    return (
      <Card container={ false } className={ cf('file-uploader', g(className)) }>
        <div className={ cf('info') }>
          <Icon name="information" />
          <Caption secondary>
            {t('SUPPORTED_FILES_FORMAT', {
              formats: this.getSupportedFilesStr(),
            })}
          </Caption>
        </div>
        <Dropzone className={ cf('dropzone-area') } activeClassName={ cf('active') } multiple={ multiple } onDrop={ this.handleDrop } disabled={ disabled }>
          {this.renderPlaceHolder()}
        </Dropzone>
        <FileQueue
          queue={ this.state.model }
          onCancelUpload={ this.handleCancelUploadingFile }
          onDeleteItem={ this.handleDeleteUploadedFile }
        />
      </Card>
    );
  }
}
