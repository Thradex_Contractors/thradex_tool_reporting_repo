import React, { Component, PropTypes } from 'react';
import { t } from 'i18next';
import { observer } from 'mobx-react';
import Text from '../typography/text';
import Icon from '../icon/icon';
import IconButton from '../icon-button/icon-button';
import { cf } from './file-queue-item.scss';
import { FileUploadProgress } from './file-upload-progress';
import { FileEntry } from './file-entry';
import clsc from '../../helpers/coalescy';

const statusClassNameHash = {
  invalid: 'invalid',
  invalidMetadata: 'invalid-metadata',
  queued: 'queued',
  uploadComplete: 'uploaded',
};

@observer
export class FileQueueItem extends Component {
  static propTypes = {
    fileEntry: PropTypes.instanceOf(FileEntry).isRequired,
    onCancelUpload: PropTypes.func,
    onDeleteItem: PropTypes.func,
  };

  handleDeleteFile = () => {
    const { fileEntry, onCancelUpload, onDeleteItem } = this.props;

    if (fileEntry.hasError || fileEntry.uploadRemoved) return;

    if (fileEntry.uploading) {
      onCancelUpload && onCancelUpload(fileEntry);
    } else {
      onDeleteItem && onDeleteItem(fileEntry);
    }
  };

  isQueueItemInvalid = fileEntry => !!(fileEntry.errorMessage || fileEntry.uploadErrorMessage || fileEntry.uploadRemoved);

  getDismissIcon = fileEntry => {
    if (this.isQueueItemInvalid(fileEntry)) return '';

    return fileEntry.uploadComplete ? 'delete' : 'close-circle';
  };

  getStatusClass = fileEntry => {
    if (this.isQueueItemInvalid(fileEntry)) return statusClassNameHash.invalid;

    const classes = [];
    if (fileEntry.uploadComplete) {
      classes.push(statusClassNameHash.uploadComplete);
    }
    if (fileEntry.metadataErrorMessage) {
      classes.push(statusClassNameHash.invalidMetadata);
      return classes.join(' ');
    }

    return fileEntry.uploadComplete ? statusClassNameHash.uploadComplete : statusClassNameHash.queued;
  };

  renderDismissButton = iconName => (
    <div className={ cf('dismiss-icon') }>
      <IconButton iconName={ iconName } onClick={ this.handleDeleteFile } />
    </div>
  );

  render({ fileEntry } = this.props) {
    const dismissIcon = this.getDismissIcon(fileEntry);
    const statusText = clsc(
      fileEntry.uploadErrorMessage,
      fileEntry.errorMessage,
      fileEntry.uploadRemoved && fileEntry.uploadComplete ? t('FILE_REMOVED') : null,
      fileEntry.uploadRemoved ? t('UPLOAD_CANCELLED') : null,
      !fileEntry.inQueue || fileEntry.uploadComplete ? `(${fileEntry.fileSizeStr})` : null,
      `(${t('FILE_IN_QUEUE')})`,
    );
    const statusClass = this.getStatusClass(fileEntry);
    const isQueueItemInvalid = this.isQueueItemInvalid(fileEntry);
    return (
      <div className={ cf('file-wrapper') }>
        {!fileEntry.uploadRemoved && fileEntry.isValid && !fileEntry.uploadComplete && <FileUploadProgress percentLoaded={ fileEntry.percentLoaded } />}
        <div className={ cf('file-item', statusClass) }>
          <div className={ cf('file-item-wrapper') }>
            <div className={ cf('file-icon') }>
              <Icon name={ fileEntry.iconName } />
            </div>
            <div className={ cf('file-name') }>
              <Text ellipsis secondary={ isQueueItemInvalid }>{fileEntry.name}</Text>
            </div>
            <div className={ cf('file-message') }>
              <Text error={ isQueueItemInvalid } secondary={ !isQueueItemInvalid }>{statusText}</Text>
            </div>
            {!!dismissIcon.length && this.renderDismissButton(dismissIcon)}
          </div>
        </div>
      </div>
    );
  }
}
