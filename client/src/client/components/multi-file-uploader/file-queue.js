import React, { PropTypes } from 'react';
import { observer } from 'mobx-react';
import { FileQueueItem } from './file-queue-item';
import { cf } from './file-queue.scss';

export const FileQueue = observer(({
  queue, onDeleteItem, onCancelUpload,
}) => {
  const ret = (
    <div className={ cf({ 'file-queue': queue.length }) }>
      {!!queue.length &&
        queue.values.map(fileEntry => (
          <FileQueueItem
            key={ fileEntry.clientId }
            fileEntry={ fileEntry }
            onDeleteItem={ onDeleteItem }
            onCancelUpload={ onCancelUpload }
          />
        ))}
    </div>
  );
  return ret;
});

FileQueue.propTypes = {
  queue: PropTypes.object.isRequired,
  onCancelUpload: PropTypes.func,
  onDeleteItem: PropTypes.func,
};
