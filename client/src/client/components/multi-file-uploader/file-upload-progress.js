import React, { PropTypes } from 'react';
import { observer } from 'mobx-react';
import { cf } from './file-upload-progress.scss';

export const FileUploadProgress = observer(({ percentLoaded, children }) => {
  if (percentLoaded > 100) {
    percentLoaded = 100;
  }

  const style = {
    width: `${percentLoaded}%`,
    transition: 'width 200ms',
  };

  return (
    <div className={ cf('file-progress') }>
      <div className={ cf('progress') } style={ style }>
        {children}
      </div>
    </div>
  );
});

FileUploadProgress.propTypes = {
  percentLoaded: PropTypes.number.isRequired,
};
