import ApiClient from '../../helpers/api-client';
import { uploadState } from './upload-state';

export const client = new ApiClient();

client.on('request:start', (e, args) => {
  uploadState.notifyStart(args);
});

client.on('request:progress', (e, args) => {
  uploadState.notifyProgress(args);
});

client.on('request:end', (e, args) => {
  uploadState.notifyEnd(args);
});
