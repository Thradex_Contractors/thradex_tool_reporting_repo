import { observable, action } from 'mobx';

export class FileMetadata {
  @observable metadata;

  constructor({ metadata = {} }) {
    this.update(metadata);
  }

  @action
  update(item) {
    this.metadata = item;
  }
}
