import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { cf, g } from './icon-button.scss';
import generateId from '../../helpers/generateId';
import Icon, { isIconAvailable } from '../icon/icon';
import Button from '../button/button';

export default class IconButton extends Component {
  static propTypes = {
    id: PropTypes.string,
    disabled: PropTypes.bool,
    className: PropTypes.string,
    iconName: PropTypes.string,
    iconStyle: PropTypes.oneOf(['light', 'dark']),
  };

  static defaultProps = {
    iconName: '',
    iconStyle: 'dark',
  };
  constructor(props, context) {
    super(props, context);
    this.id = generateId(this);
  }

  render() {
    /* esfmt-ignore-start */
    const { iconName, className, iconStyle, disabled, noHover, compact, iconProps, ...btnProps } = this.props;
    /* esfmt-ignore-end */

    const useLight = iconStyle === 'light' && !disabled;

    const cNames = cf(
      'icon-button',
      {
        light: useLight,
        noHover,
        compact,
        loading: btnProps.loading,
      },
      g(className),
    );

    let { children } = this.props;

    if (!children) {
      if (!isIconAvailable(iconName)) {
        children = <i className={ cf('icon', g('material-icons')) }>{iconName}</i>;
      } else {
        children = <Icon name={ iconName } iconStyle={ iconStyle } { ...iconProps } />;
      }
    }

    return (
      <Button className={ cNames } type="wrapper" disabled={ disabled } loaderStyle={ iconStyle } data-component="icon-button" { ...btnProps }>
        <span className={ cf('wrapper') }>{children}</span>
      </Button>
    );
  }
}
