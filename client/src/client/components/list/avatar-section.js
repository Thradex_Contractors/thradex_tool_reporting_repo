import React from 'react';
import { cf, g } from './list.scss';

export default function AvatarSection({ className, children, ...props }) {
  return (
    <div className={ cf('avatar-section', g(className)) } { ...props }>
      {children}
    </div>
  );
}
