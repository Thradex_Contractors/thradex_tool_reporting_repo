import React from 'react';
import { cf, g } from './list.scss';

const Divider = ({ className, children, ...props }) => (
  <div data-component="divider" className={ cf('divider', g(className)) } { ...props }>
    {children}
  </div>
);

export default Divider;
