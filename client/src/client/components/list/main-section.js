import React from 'react';
import { cf, g } from './list.scss';

export default function MainSection({ className, children, ...props }) {
  return (
    <div className={ cf('main-section', g(className)) } { ...props }>
      <div>{children}</div>
    </div>
  );
}
