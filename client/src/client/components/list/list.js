import React, { Component } from 'react';
import { cf, g } from './list.scss';

// since we need to get a ref to this component
// we cannot make it a stateless component
// eslint-disable-next-line react/prefer-stateless-function
export default class List extends Component {
  render() {
    const { className, children, ...props } = this.props;
    return (
      <div data-component="list" className={ cf('list', g(className)) } { ...props }>
        {children}
      </div>
    );
  }
}
