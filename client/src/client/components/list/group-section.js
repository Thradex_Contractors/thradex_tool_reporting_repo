import React from 'react';
import MainSection from './main-section';
import { cf, g } from './list.scss';

export default function GroupSection({ className, noIndentGroupItems, children, ...props }) {
  return (
    <div className={ cf('group-section', { noIndentGroupItems }, g(className)) } { ...props }>
      <MainSection>{children}</MainSection>
    </div>
  );
}
