import React, { Children } from 'react';
import { cf, g } from './list.scss';
import AvatarSection from './avatar-section';
import ActionSection from './action-section';

// rowStyle [row-simple, row-mixed]
export default function ListItem({
  className,
  clickable = true,
  hoverable = true,
  fixedSections,
  selected,
  disabled,
  onClick,
  rowStyle = 'simple',
  children,
  ...props
}) {
  if (typeof fixedSections === 'undefined') {
    fixedSections = 0;

    Children.forEach(children, item => {
      if (item && (item.type === AvatarSection || item.type === ActionSection)) {
        fixedSections++;
      }
    });
  }

  const cNames = cf(
    'list-item',
    {
      hoverable,
      disabled,
      clickable,
      // every ListItem can contain one or two fixed sections
      // one for the Avatar, and one for an action icon
      // these clases are used to fix an issue that happen when
      // the content inside the container with `flex:1` uses nowrap.
      //
      // As described here: https://redisrupt.atlassian.net/browse/CPM-4104
      // the issue is that the no wrapped text breaks the layout preventing
      // the container to have the right size.
      //
      // This issue does not happen if the content inside does not use wrap
      'one-fixed': fixedSections === 1,
      'two-fixed': fixedSections === 2,
      selected,
    },
    rowStyle,
    g(className),
  );

  return (
    <div data-component="list-item" onClick={ e => !disabled && (onClick && onClick(e)) } className={ cNames } { ...props }>
      {children}
    </div>
  );
}
