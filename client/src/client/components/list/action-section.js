import React from 'react';
import { cf, g } from './list.scss';

export default function ActionSection({ className, children, ...props }) {
  return (
    <div className={ cf('action-section', g(className)) } { ...props }>
      {children}
    </div>
  );
}
