import React from 'react';
import { cf, g } from './fly-out.scss';

export default function FlyOutActions({ children, className, ...props }) {
  return (
    <div className={ cf('flyout-actions', g(className)) } { ...props }>
      {children}
    </div>
  );
}
