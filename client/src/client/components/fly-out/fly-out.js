import React, { Component, Children, cloneElement, PropTypes } from 'react';
import {
  unstable_renderSubtreeIntoContainer, // eslint-disable-line
  unmountComponentAtNode,
  findDOMNode
} from 'react-dom';

import $ from 'jquery';
import '../../helpers/window-resize';

import generateId from '../../helpers/generateId';
import { document } from '../../../../config/common/globals';

import { cf } from './fly-out.scss';

import FlyOutOverlay from './fly-out-overlay';
import clsc from '../../helpers/coalescy';

const positionByExpandLocation = {
  bottom: {
    pos: {
      my: 'center top',
      at: 'center bottom',
    },
  },
  'bottom-right': {
    pos: {
      my: 'left top',
      at: 'left bottom',
    },
  },
  'bottom-left': {
    pos: {
      my: 'right top',
      at: 'right bottom',
    },
  },
  top: {
    pos: {
      my: 'center bottom',
      at: 'center top',
    },
  },
  'on-top': {
    pos: {
      my: 'center center',
      at: 'center center',
    },
  },
  'top-right': {
    pos: {
      my: 'left bottom',
      at: 'left top',
    },
  },
  'top-left': {
    pos: {
      my: 'right bottom',
      at: 'right top',
    },
  },
  right: {
    pos: {
      my: 'left center',
      at: 'right center',
    },
  },
  'right-bottom': {
    pos: {
      my: 'left top',
      at: 'right top',
    },
  },
  'right-top': {
    pos: {
      my: 'left bottom',
      at: 'right bottom',
    },
  },
  left: {
    pos: {
      my: 'right center',
      at: 'left center',
    },
  },
  'left-bottom': {
    pos: {
      my: 'right top',
      at: 'left top',
    },
  },
  'left-top': {
    pos: {
      my: 'right bottom',
      at: 'left bottom',
    },
  },
  'over-bottom': {
    pos: {
      my: 'center top',
      at: 'center top',
    },
  },
  'over-bottom-right': {
    pos: {
      my: 'left top',
      at: 'left top',
    },
  },
  'over-bottom-left': {
    pos: {
      my: 'right top',
      at: 'right top',
    },
  },
  'over-top': {
    pos: {
      my: 'center bottom',
      at: 'center bottom',
    },
  },
  'over-top-right': {
    pos: {
      my: 'left bottom',
      at: 'left bottom',
    },
  },
  'over-top-left': {
    pos: {
      my: 'right bottom',
      at: 'right bottom',
    },
  },
  'over-right': {
    pos: {
      my: 'left center',
      at: 'left center',
    },
  },
  'over-right-bottom': {
    pos: {
      my: 'left top',
      at: 'left top',
    },
  },
  'over-right-top': {
    pos: {
      my: 'left bottom',
      at: 'left bottom',
    },
  },
  'over-left': {
    pos: {
      my: 'right center',
      at: 'right center',
    },
  },
  'over-left-bottom': {
    pos: {
      my: 'right top',
      at: 'right top',
    },
  },
  'over-left-top': {
    pos: {
      my: 'right bottom',
      at: 'right bottom',
    },
  },
};

export default class FlyOut extends Component { // eslint-disable-line

  static propTypes = {
    id: PropTypes.string,
    expandTo: PropTypes.string,
    onPosition: PropTypes.func,
    onOpening: PropTypes.func,
    onOpen: PropTypes.func,
    onClose: PropTypes.func,
    onClosing: PropTypes.func,
    positionArgs: PropTypes.object,
    closeOnTapAway: PropTypes.bool,
    useHover: PropTypes.bool,
    hoverDelay: PropTypes.number,
    appendToBody: PropTypes.bool,
    open: PropTypes.bool,
    onCloseRequest: PropTypes.func,
    zIndex: PropTypes.number,
  };

  static defaultProps = {
    hoverDelay: 800,
    useHover: false,
    expandTo: 'bottom',
    closeOnTapAway: true,
  };

  constructor(props) {
    super(props);
    this.id = generateId(this);
    this.$doc = $(document);

    this._wasOpenAtConstructor = !!props.open;

    this.state = {
      open: props.open,
    };
  }

  state = {
    open: false,
  };


  componentDidMount() {
    this._mounted = true;

    const { appendToBody, onCloseRequest, usePrevSiblingAsTrigger, noAutoBind, modal } = this.props;

    this._trigger = !usePrevSiblingAsTrigger ? findDOMNode(this) : $(findDOMNode(this)).prev()[0];

    const [theChild] = Children.toArray(this.props.children).filter(child => child.type !== FlyOutOverlay);

    if (theChild && !noAutoBind) {
      this._bindTriggers(this._trigger);
    }

    const parentNode = appendToBody ? 'body' : this._trigger.parentNode;

    if (modal) {
      this.modalOverlay = $(`<div class=${cf('modal-overlay')} />`).appendTo(parentNode);
    }

    this.mountPoint = $(`<div class="${cf('flyout-container')}" />`).appendTo(parentNode);
    this.mountPoint.on(`window:resize.ns_${this.id}`, () => {
      this._renderOverlay();
    });

    this.mountPoint.on(`mouseup.ns_${this.id} keyup._ns${this.id}`, '[data-action="close"]', e => {
      // this is required to prevent a FlyOut inside a FlyOut to close all the flyouts
      // instead of just closing itself
      e.stopPropagation && e.stopPropagation();

      if (e.type === 'keyup' && e.keyCode !== 13) {
        return;
      }
      const $target = $(e.target);
      if ($target.is(':disabled')) {
        return; // do nothing
      }

      if (onCloseRequest) {
        onCloseRequest({ source: 'dataAction', target: e.target });
        return;
      }

      this.close();
    });

    this._renderOverlay();
  }

  componentWillReceiveProps(nextProps) {
    if ('open' in nextProps && nextProps.open !== this.props.open) {
      if (nextProps.open) {
        this.open();
      } else {
        this.close();
      }
    }
  }

  componentDidUpdate() {
    this._renderOverlay();
  }

  componentWillUnmount() {
    this._mounted = false;

    unmountComponentAtNode(this.mountPoint[0]);
    this.mountPoint.off(`.ns_${this.id}`);
    this.mountPoint.remove();
    this.modalOverlay && this.modalOverlay.remove();
    this.modalOverlay = null;
    this.mountPoint = null;
    this.$doc.off(`.ns_${this.id}`);
    this.$doc = null;

    this._unbindTriggers();
    this._trigger = null;
  }

  safeSetState(...args) {
    if (!this._mounted) {
      return;
    }
    this.setState(...args);
  }

  open() {
    if (this.state.open) {
      return;
    }

    // needed to determine if we need to fire onClose event
    // we assume that the first time we don't need so we
    // make sure we have displayed the FlyOut at least once
    if (!this._openedAtLeastOnce) {
      this._openedAtLeastOnce = true;
    }

    this.safeSetState({
      open: true,
    });

    const { onOpening } = this.props;

    setTimeout(() => onOpening && onOpening(), 0);
  }

  get isOpen() {
    return this.state.open;
  }

  close() {
    if (!this.state.open) {
      return;
    }

    this.safeSetState({
      open: false,
    });

    const { onClosing } = this.props;
    setTimeout(() => onClosing && onClosing(), 0);
  }

  toggle() {
    if (this.state.open) {
      this.close();
    } else {
      this.open();
    }
  }

  _onComplete() {
    const { open } = this.state;
    const { closeOnTapAway, onClose, onOpen, onCloseRequest } = this.props;

    if (!open) {
      this.mountPoint.removeClass(cf('open'));
      this._openedAtLeastOnce && setTimeout(() => onClose && onClose(), 0);
      this.$doc.off(`mouseup.ns_${this.id}`);
    } else {
      setTimeout(() => onOpen && onOpen(), 0);

      if (!closeOnTapAway) {
        return;
      }

      const ref = this._trigger ? $(this._trigger) : null;
      // the following listener is added to capture the mouseup event in the
      // entire page. We use the target to discern whether the events happened
      // inside the overlay or inside the element used as the trigger. If that's the
      // case we ignore the event. But if the event happened outside then
      // we close the overlay
      this.$doc.on(`mouseup.ns_${this.id}`, e => {
        const $target = $(e.target);
        const eventHappenedInsideOverlay = $target.closest(this.mountPoint).length > 0;
        const eventHappenedInsideTrigger = ref && $target.closest(ref).length > 0;

        if (eventHappenedInsideOverlay || eventHappenedInsideTrigger) {
          return;
        }

        if (this.state.open && onCloseRequest) {
          onCloseRequest({
            source: 'tapAway',
            target: e.target,
            button: e.button,
          });
          return;
        }

        this.close();
      });
    }
  }

  _renderOverlay() {
    const [overlay] = Children.toArray(this.props.children).filter(child => child.type === FlyOutOverlay);

    if (!overlay) {
      return;
    }

    let { open } = this.state;
    const { expandTo, delayToApplyPosition = 16, onPosition, positionArgs, onOpening, zIndex, overTrigger, modal, matchTriggerSize = true } = this.props;

    if (this._wasOpenAtConstructor) {
      if (open) {
        setTimeout(() => {
          onOpening && onOpening();
        }, 0);
      }
      this._wasOpenAtConstructor = false;
    }

    open = !!open;

    const onComplete = () => {
      if (!this._mounted) {
        return;
      }
      this._onComplete();
    };

    unstable_renderSubtreeIntoContainer(this, cloneElement(overlay, { ...overlay.props, onComplete, open, expandTo }), this.mountPoint[0]);

    if (open) {
      const ref = this._trigger ? $(findDOMNode(this._trigger)) : null;
      const { pos } = positionByExpandLocation[`${overTrigger ? 'over-' : ''}${expandTo}`];

      modal && this.modalOverlay.addClass(cf('open'));
      this.mountPoint.addClass(cf('open'));
      if (typeof zIndex === 'number') {
        if (modal) {
          this.modalOverlay.css({ zIndex });
        }
        this.mountPoint.css({ zIndex });
      }

      if (ref) {
        const btnWidth = ref.outerWidth();
        const overlayWidth = this.mountPoint.outerWidth();

        if (matchTriggerSize) {
          if (btnWidth >= overlayWidth) {
            this.mountPoint.width(btnWidth);
          } else {
            this.mountPoint.css('width', '');
          }
        }
      }

      const pArgs = positionArgs || {};
      const args = {
        position: {
          my: clsc(pArgs.my, pos.my),
          at: clsc(pArgs.at, pos.at),
          of: clsc(pArgs.of, ref),
          collision: clsc(pArgs.collision, 'flipfit'),
        },
        $trigger: ref,
        autoPosition: true,
        $overlay: this.mountPoint,
      };

      setTimeout(() => {
        if (!this._mounted) return;
        onPosition && onPosition(args);
        if (args.autoPosition) {
          this.mountPoint.position(args.position);
        }
      }, delayToApplyPosition);
    } else {
      modal && this.modalOverlay.removeClass(cf('open'));
      this.$doc.off(`.ns_${this.id}`);
    }
  }

  _bindTriggers(trigger) {
    const { useHover, hoverDelay } = this.props;

    const $trigger = (this.$trigger = $(trigger));
    if (!useHover) {
      $trigger.on(`click.ns_${this.id}`, () => this.toggle());
    } else {
      $trigger.on(`mouseover.ns_${this.id}`, () => {
        clearTimeout(this._lastOpenTimerId);
        this._lastOpenTimerId = setTimeout(() => this.open(), hoverDelay);
      });
      $trigger.on(`mouseout.ns_${this.id} click.ns_${this.id}`, () => {
        clearTimeout(this._lastOpenTimerId);
        this.close();
      });
    }
  }

  _unbindTriggers() {
    if (this.$trigger) {
      this.$trigger.off(`.ns_${this.id}`);
      this.$trigger = null;
    }
  }

  render() {
    const [theChild] = Children.toArray(this.props.children).filter(child => child.type !== FlyOutOverlay);

    if (!theChild) {
      return <noscript />; // just render something that doesn't have a visual rep.
    }

    return theChild;
  }
}
