import React from 'react';
import { cf } from './generic-fly-out-container.scss';

const GenericFlyOutContainer = ({ children }) => (
  <div id="flyoutContainer" className={ cf('docked-flyout-container on') }>
    {children}
  </div>
);

export default GenericFlyOutContainer;
