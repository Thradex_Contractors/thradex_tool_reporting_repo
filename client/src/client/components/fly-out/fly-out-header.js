import React from 'react';
import { cf, g } from './fly-out.scss';
import Title from '../typography/title';

export default function FlyOutHeader({ className, title, fullscreen, children, ...props }) {
  const titleC = typeof title === 'string' ? <Title>{title}</Title> : title;

  return (
    <div className={ cf('header', { fullscreen }, g(className)) } { ...props }>
      {titleC}
      {children}
    </div>
  );
}
