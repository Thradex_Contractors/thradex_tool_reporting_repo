import React from 'react';
import { cf, g } from './section.scss';
import Text from '../typography/text';

const SectionTitle = ({ children, actionItems, className }) => {
  let text;
  if (typeof children === 'string') {
    text = (
      <div>
        <Text bold>{children}</Text>
      </div>
    );
  } else {
    text = children;
  }

  return (
    <div className={ cf('title-section', g(className)) }>
      {text} {actionItems && <div className={ cf('action-items') }>{actionItems}</div>}
    </div>
  );
};

export default SectionTitle;
