import React from 'react';
import { cf, g } from './section.scss';
import SectionTitle from './section-title';
import { toTitleCase } from '../../helpers/capitalize';
import Caption from '../typography/caption';

const Section = ({ children, className, fullWidth = true, title, helperText, padContent = true, actionItems, ...props }) => {
  const cNames = cf('section', { fullWidth, padContent }, g(className));
  let labelC;

  if (typeof title === 'string') {
    labelC = title && <SectionTitle actionItems={ actionItems }>{toTitleCase(title)}</SectionTitle>;
  } else {
    labelC = title;
  }

  if (typeof helperText === 'string') {
    helperText = helperText && (
      <Caption secondary className={ cf('helper-text') }>
        {helperText}
      </Caption>
    );
  }

  return (
    <div data-component="section" className={ cNames } { ...props }>
      {labelC}
      {helperText}
      <div className={ cf('content') }>{children}</div>
    </div>
  );
};

export default Section;
