import React from 'react';
import { cf, g } from './card.scss';
import elevationShadow from '../../helpers/elevation-shadow';

const Card = ({ className, children, elevation = 2, container = true, style = {}, ...rest }) => {
  const overlayStyle = {
    ...style,
    boxShadow: elevation === 0 ? 'none' : elevationShadow(elevation),
  };

  return (
    <div
      data-component="card"
      className={ cf('card', g(className), {
        container,
      }) }
      style={ overlayStyle }
      { ...rest }>
      { children }
    </div>
  );
};

export default Card;
