import createElement from './create-element';

const Caption = createElement('caption');

export default Caption;
