import createElement from './create-element';

const Title = createElement('title');

export default Title;
