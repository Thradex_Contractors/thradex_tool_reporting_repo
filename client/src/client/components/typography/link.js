import React from 'react';
import { cf, g } from './typography.scss';

const Link = ({ id, className, noDefaultColor, underline, href, children, ...rest }) => (
  <a href={ href } id={ id } className={ cf('link', { noDefaultColor, underline }, g(className)) } { ...rest }>
    <span>{children}</span>
  </a>
);

export default Link;
