export Text from './text';
export TextHeavy from './text-heavy';
export Caption from './caption';
export Title from './title';
export Headline from './headline';
export SubHeader from './sub-header';
export Link from './link';
