import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { cf, g } from './icon.scss';
import svgs, { icons } from './svgs';

export const isIconAvailable = name => !!svgs[name];

export default class Icon extends Component {
  static propTypes = {
    className: PropTypes.string,
    name(props, propName, componentName) { // eslint-disable-line
      const propVal = props[propName];
      if (!icons.includes(propVal)) {
        return new Error(`"${propVal}" is not a valid value for the "name" prop in "${componentName}". Validation failed`);
      }
    },
    iconStyle: PropTypes.oneOf(['light', 'dark']),
    style: PropTypes.object,
  };

  render() {
    const {
      className,
      name,
      disabled,
      iconStyle,
      style,
    } = this.props;

    const iconName = isIconAvailable(name) ? name : 'missing-icon';

    const iconClasses = cf('icon', {
      light: iconStyle === 'light',
      disabled,
    }, g(className));

    const svgIcon = { __html: svgs[iconName] };
    /*eslint-disable */
    return (
      <div className={ iconClasses } style={ style } data-component='icon'>
        <svg width="24"
            height="24"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg">
            <g id={ iconName } dangerouslySetInnerHTML={ svgIcon } />
        </svg>
      </div>
    );
    /*eslint-disable */
  }
}
