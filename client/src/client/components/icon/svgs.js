import svgs from '../../helpers/svg/sprite';

export const icons = Object.keys(svgs);
export default svgs;
