import React, { Component, PropTypes } from 'react';
import $ from 'jquery';
import clsc from '../../helpers/coalescy';
import FlyOut from '../fly-out/fly-out';
import FlyOutOverlay from '../fly-out/fly-out-overlay';
import List from '../list/list';

import generateId from '../../helpers/generateId';

export default class Menu extends Component {
  static propTypes = {
    id: PropTypes.string,
    menuListClassName: PropTypes.string,
    iconClassName: PropTypes.string,
    iconName: PropTypes.string,
    iconStyle: PropTypes.string,
    trigger: PropTypes.object,
    expandTo: PropTypes.string,
    positionArgs: PropTypes.object,
    buttonStyle: PropTypes.object,
    menuListStyle: PropTypes.object,
  };

  static defaultProps = {
    expandTo: 'bottom-right',
    positionArgs: {
      my: 'right top',
      at: 'right top',
    },
  };

  constructor(props) {
    super(props);
    this.id = generateId(this);
  }

  raiseSelect = e => {
    const { onSelect, onCloseRequest } = this.props;

    const $item = $(e.target).closest('[data-menu-item="true"]');
    if ($item.length > 0) {
      if ($item.attr('data-disabled') === 'true') {
        onCloseRequest && onCloseRequest({ source: 'clickOnDisabledItem' });
        // do not consider disabled items
        return;
      }

      const args = {
        autoClose: true,
        closeMenu: () => onCloseRequest && onCloseRequest({ source: 'closeMenu' }),
        action: $item.attr('data-action'),
      };

      onSelect && onSelect(args);

      if (args.autoClose) {
        onCloseRequest && onCloseRequest({ source: 'autoClose' });
      }
    }
  };

  render() {
    const { id, children, expandTo, positionArgs, onCloseRequest, open, trigger, noAutoBind, menuListClassName, menuListStyle, ...rest } = this.props;

    const style = {
      whiteSpace: 'nowrap',
      minWidth: 170, // as requested by Ityam
      ...menuListStyle,
    };

    const theId = clsc(id, this.id);

    return (
      <FlyOut id={ theId } open={ open } onCloseRequest={ onCloseRequest } expandTo={ expandTo } noAutoBind={ noAutoBind } positionArgs={ positionArgs } { ...rest }>
        {trigger}
        <FlyOutOverlay container={ false } elevation={ 2 }>
          <List style={ style } className={ menuListClassName } onClick={ this.raiseSelect }>
            {children}
          </List>
        </FlyOutOverlay>
      </FlyOut>
    );
  }
}
