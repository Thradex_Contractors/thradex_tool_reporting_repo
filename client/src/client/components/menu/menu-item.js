import React, { Component, PropTypes } from 'react';
import ListItem from '../list/list-item';
import MainSection from '../list/main-section';
import AvatarSection from '../list/avatar-section';
import Text from '../typography/text';
import Icon from '../icon/icon';

export default class MenuItem extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    className: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    icon: PropTypes.object,
    iconName: PropTypes.string,
    action: PropTypes.string,
  };

  static defaultProps = {
    disabled: false,
  };

  handleClick = () => {
    const { onClick, disabled } = this.props;
    if (disabled) {
      return;
    }

    onClick && onClick();
  };

  render() {
    const { className, icon, iconName, text, action, disabled, ...rest } = this.props;
    let theIcon = icon;
    if (!theIcon) {
      if (iconName) {
        theIcon = <Icon name={ iconName } />;
      }
    }
    return (
      <ListItem
        className={ className }
        data-menu-item={ true }
        onClick={ this.handleClick }
        disabled={ disabled }
        data-disabled={ disabled }
        data-action={ action }
        { ...rest }>
        {theIcon && <AvatarSection>{theIcon}</AvatarSection>}
        <MainSection>
          <Text>{text}</Text>
        </MainSection>
      </ListItem>
    );
  }
}
