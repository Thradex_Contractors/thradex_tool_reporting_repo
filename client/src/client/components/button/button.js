import React, { Component, PropTypes } from 'react';
import { observer } from 'mobx-react';
import { cf, g } from './button.scss';
import clsc from '../../helpers/coalescy';
import generateId from '../../helpers/generateId';
import LoaderIndicator from '../loader-indicator/loader-indicator';
const { bool, string, oneOf } = PropTypes;

@observer
export default class Button extends Component {
  static propTypes = {
    useWaves: bool,
    type: string,
    wavesStyle: string,
    isSubmit: bool,
    loading: bool,
    wide: bool,
    loaderStyle: string,
    btnRole: oneOf(['primary', 'secondary']), // if not passed assumed primary
    useDisable: bool,
  };

  static defaultProps = {
    useWaves: true,
    type: 'raised',
    wavesStyle: '',
    isSubmit: false,
    useDisable: false,
  };

  constructor(props) {
    super(props);
    this.id = generateId(this);
    this.state = {};
  }

  handleClick = e => this.fireClick(e);

  fireClick(e) {
    const { onClick, disabled, loading } = this.props;

    if (disabled || loading) {
      return;
    }

    onClick && onClick(e);
  }

  focus() {
    this.btn && this.btn.focus();
  }

  render() {
    const { props } = this;
    /* esfmt-ignore-start */
    const {
      type,
      isSubmit,
      className,
      disabled,
      useDisable,
      size,
      useWaves,
      label,
      wide,
      loading,
      loaderStyle,
      wavesStyle,
      btnRole: theBtnRole,
      ...btnProps
    } = this.props;
    /* esfmt-ignore-end */

    const id = clsc(props.id, this.id);

    let wStyles = wavesStyle;

    if (!wStyles) {
      wStyles = type === 'flat' || (type === 'raised' && theBtnRole === 'secondary') ? 'light' : 'dark';
    }

    const cxObj = {
      'btn-flat': type === 'flat',
      btn: type === 'raised',
      'btn-floating': type === 'floating',
      disabled: disabled || useDisable,
      'btn-large': size === 'large',
      'waves-effect': useWaves && !disabled,
      'waves-light': wStyles === 'light',
    };

    let btnRole = (props.btnRole || '').trim();

    if (!btnRole) {
      btnRole = 'primary';
    }

    const cNames =
      type === 'wrapper'
        ? className
        : cf(
          g(cxObj, className),
          {
            wide,
            'btn-flat': type === 'flat',
            primary: btnRole === 'primary',
            secondary: btnRole === 'secondary',
            loading,
          },
          'custom-btn',
        );

    const children = this.props.children || <span>{label}</span>;
    const buttonType = isSubmit ? 'submit' : 'button';

    let loadingC;
    if (loading) {
      let darkerMode;

      if (!loaderStyle) {
        darkerMode = type === 'flat' || btnRole === 'secondary';
      } else {
        darkerMode = loaderStyle === 'darker';
      }

      loadingC = <LoaderIndicator darker={ darkerMode } />;
    }

    return (
      <button
        ref={ btn => { this.btn = btn; } }
        data-component="button"
        { ...btnProps }
        id={ id }
        role="button"
        data-button-type={ type }
        type={ buttonType }
        onClick={ this.handleClick }
        className={ cNames }
        disabled={ disabled }>
        {children}
        {loadingC}
      </button>
    );
  }
}
