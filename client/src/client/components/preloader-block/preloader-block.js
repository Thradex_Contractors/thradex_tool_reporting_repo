import React from 'react';
import { cf, g } from './preloader-block.scss';
import Preloader from '../preloader/preloader';
import Text from '../typography/text';

export default ({ id, className, size, style, modal, message }) => (
  <div id={ id } className={ cf('preloader-block', { modal }, g(className)) } style={ style }>
    {message && <Text className={ cf('message') }>{message}</Text>}
    <Preloader size={ size } />
  </div>
);
