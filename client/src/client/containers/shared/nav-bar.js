import React from 'react';
import { Icon } from '../../components';
import { cf, g } from './nav-bar.scss';

const NavBar = ({ title, iconName = 'home' }) =>
  <header className={ cf(g('nabvar', 'navbar-fixed')) }>
    <nav className={ cf(g('blue', 'darken-2')) }>
      <div className={ cf(g('nav-wrappe')) }>
        <a className={ cf('brand-logo') }><Icon name={ iconName }/><span className={ cf('title') }>{ title }</span></a>
      </div>
    </nav>
  </header>;

export default NavBar;
