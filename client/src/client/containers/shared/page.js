import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Layout } from '../../components';

const { Header, Footer, Content } = Layout;

export default class Page extends Component {
  static propTypes = {
    header: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.string,
    ]),
    footer: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.string,
    ]),
  };

  renderHeader = () => {
    const { header } = this.props;
    if (!header) return null;

    return typeof header === 'string' ? <Header>{ header }</Header> : header;
  }

  renderFooter = () => {
    const { footer } = this.props;
    if (!footer) return null;

    return typeof footer === 'string' ? <Footer>{ footer }</Footer> : footer;
  }

  render() {
    return (
      <Layout>
        { this.renderHeader() }
        <Content>
          { this.props.children }
        </Content>
        { this.renderFooter() }
      </Layout>
    );
  }
}
