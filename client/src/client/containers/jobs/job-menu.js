import React from 'react';
import { t } from 'i18next';
import { CardMenu, CardMenuItem } from '../../components';

const JobMenu = ({
  hasActiveJob,
  onCreateNewJob,
}) => (
  <CardMenu iconName="dots-vertical">
    <CardMenuItem text={ t('NEW_JOB') } onClick={ onCreateNewJob } disabled={ hasActiveJob } />
  </CardMenu>
);

export default JobMenu;
