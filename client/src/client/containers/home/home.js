import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { t } from 'i18next';
import Page from '../shared/page';
import NavBar from '../shared/nav-bar';
import { loadCurrentJob, createJob } from '../../redux/modules/job';
import {
  Button,
  MultiFileUploader,
  Section,
  // SectionTitle,
  PreloaderBlock,
  Typography as T
} from '../../components';
// import JobMenu from '../jobs/job-menu';
import { DOCTypes } from '../../helpers/file-type';
import { cf } from './home.scss';
import config from '../../config';

@connect(
  state => ({
    currentJob: state.jobStore.currentJob,
    loading: state.jobStore.loading,
    error: state.jobStore.error,
  }),
  dispatch => bindActionCreators({
    loadCurrentJob,
    createJob,
  }, dispatch)
)
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.loadCurrentJob();
  }

  handleOnClick = () => this.props.login('javier', 'my password');

  handleOnCreateNewJob = () => this.props.createJob();

  renderHeader = () => <NavBar title={ config.app.name } iconName="property" />;

  renderCurrentJob = () => {
    const { currentJob, loading } = this.props;
    if (loading) <PreloaderBlock message={ t('NEW_JOB_MESSAGE') } />;

    if (!currentJob) {
      return (
        <div className={ cf('empty-job') }>
          <T.Text secondary>{t('NO_CURRENT_JOB_MESSAGE')}</T.Text>
        </div>
      );
    }

    return (
      <div>
        <T.Text> secondaryMissing dessign for current job</T.Text>
        <Button label="Test Request" onClick={ this.handleOnClick } />
      </div>
    );
  }

  render() {
    const hasActiveJob = !!this.props.currentJob;

    return (
      <Page header={ this.renderHeader() }>
        {/* <Section
          padContent={ true }
          title={
            <SectionTitle
              actionItems={ <JobMenu hasActiveJob={ hasActiveJob } onCreateNewJob={ this.handleOnCreateNewJob } /> }>
              { t('CURRENT_JOB') }
            </SectionTitle>
          }>
          { this.renderCurrentJob() }
        </Section> */}
        <Section padContent={ true } title={ t('DOCUMENTS') } helperText={ t('DOCUMENTS_HELP_TEXT') }>
          <MultiFileUploader
            className={ cf('upload-documents') }
            uploadPath="documents/upload"
            fileSize={ 10 }
            disabled={ !hasActiveJob }
            supportedFileFormats={ [DOCTypes.xls, DOCTypes.xlsx] } />
        </Section>
      </Page>
    );
  }
}
