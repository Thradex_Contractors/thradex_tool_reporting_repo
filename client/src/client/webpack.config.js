import { makeConfig } from '../../config/webpack/webpack-config-factory';
import { root } from '../../config/webpack/helpers';
import config from './config';

const getRoot = fileName => root('src', 'client', fileName);

export default makeConfig({
  env: config.env,
  entry: {
    polyfills: getRoot('polyfills.js'),
    vendor: getRoot('vendor.js'),
    client: getRoot('client.js'),
  },
  rootPath: root('src', 'client'),
  indexPath: getRoot('index.html'),
  outputPath: root('dist'),
  metadata: {
    title: config.app.name,
    isProduction: config.isProduction,
  },
});
