import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';
import createStore from './redux/create';
import getRoutes from './routing';
import { client } from './modules/api-client';
import { initTrans } from './helpers/i18n-client';
import './assets/sass/site.scss';
import './helpers/position';

const routingMiddleware = routerMiddleware(browserHistory);
const store = createStore(client, routingMiddleware, {}); // window.__data)
const history = syncHistoryWithStore(browserHistory, store);
const appContainer = document.getElementById('app');

const component = (
  <Router history={ history }>
    { getRoutes(store) }
  </Router>
);

const renderApp = () => {
  let appContent = <div>{ component }</div>;
  if (__DEVTOOLS__ && !window.devToolsExtension) {
    const { DevTools } = require('./helpers/dev-tools/dev-tools'); // eslint-disable-line
    appContent = <div>
      { component }
      <DevTools />
    </div>;
  }

  ReactDOM.render(
    <Provider store={ store } key="provider">
      { appContent }
    </Provider>,
    appContainer
  );
};

const initClient = async () => {
  initTrans(renderApp);
};

initClient().catch(console.error.bind(console, 'client-main:error'));
