const LOAD_JOB = 'job/LOAD_JOB';
const LOAD_JOB_SUCCESS = 'job/LOAD_JOB_SUCCESS';
const LOAD_JOB_FAIL = 'job/LOAD_JOB_FAIL';

const CREATE_JOB = 'job/CREATE_JOB';
const CREATE_JOB_SUCCESS = 'job/CREATE_JOB_SUCCESS';
const CREATE_JOB_FAIL = 'job/CREATE_JOB_FAIL';

const initialstate = {
  currentJob: { id: 'id988' },
  loading: false,
  error: '',
};

export default (state = initialstate, action) => {
  switch (action.type) {
    case LOAD_JOB:
      return state;
    case LOAD_JOB_SUCCESS:
      return state;
    case LOAD_JOB_FAIL:
      return state;
    case CREATE_JOB:
      return {
        ...state,
        loading: true,
      };
    case CREATE_JOB_SUCCESS: {
      const currentJob = action.result;
      return {
        ...state,
        loading: false,
        currentJob,
      };
    }
    case CREATE_JOB_FAIL: {
      const { error } = action.result; // TODO: check errors
      return {
        ...state,
        loading: false,
        currentJob: null,
        error,
      };
    }
    default:
      return state;
  }
};

// Fake -
export const loadCurrentJob = () => ({
  type: LOAD_JOB_SUCCESS,
});

// Real
// export const loadCurrentJob = () => ({
//   types: [LOAD_JOB, LOAD_JOB_SUCCESS, LOAD_JOB_FAIL],
//   promise: client => client.get('/jobs/currentJob'),
// });

export const createJob = () => ({
  types: [CREATE_JOB, CREATE_JOB_SUCCESS, CREATE_JOB_FAIL],
  promise: client => client.post('/jobs', { data: { test: 'data test' } }),
});

