import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import authStore from './auth';
import jobStore from './job';

export default combineReducers({
  routing: routerReducer,
  authStore,
  jobStore,
});
