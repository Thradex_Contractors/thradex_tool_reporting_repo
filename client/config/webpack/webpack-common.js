import webpack from 'webpack';
import ProgressPlugin from 'webpack/lib/ProgressPlugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import AssetsPlugin from 'assets-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import autoprefixer from 'autoprefixer';
import path from 'path';
import { root } from './helpers';

const webpackConfig = ({ entry, rootPath, indexPath, outputPath, metadata }) => {
  const definePluginOptions = {
    __CLIENT__: true,
    __DEVELOPMENT__: true,
    __DEVTOOLS__: true,
  };

  const scssLoader = {
    test: /\.scss$/,
    exclude: /node_modules/,
    use: [
      { loader: 'css-local-loader' },
      { loader: 'style-loader', options: { sourceMap: true } },
      { loader: 'css-loader', options: { sourceMap: true, modules: true, minimize: false, importLoaders: 1, localIdentName: '[local]_[hash:base64:5]' } },
      { loader: 'sass-loader', options: { sourceMap: true, outputStyle: 'expanded', sourceMapContents: true } },
    ],
  };

  const cssLoader = {
    test: /\.css$/,
    use: [{ loader: 'style-loader', options: { sourceMap: true } }, { loader: 'css-loader', options: { sourceMap: true } }],
  };

  if (metadata.isProduction) {
    scssLoader.use = ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: [
        { loader: 'css-loader', options: { sourceMap: true, modules: true, importLoaders: 2 } },
        { loader: 'postcss-loader', options: { sourceMap: true, plugins: [autoprefixer({ browsers: ['last 2 versions'] })] } },
        { loader: 'sass-loader', options: { sourceMap: true, outputStyle: 'expanded', sourceMapContents: true } },
      ],
    });

    cssLoader.use = ExtractTextPlugin.extract({ fallback: 'style-loader', use: [{ loader: 'css-loader' }] });
  }

  const config = {
    entry,
    resolve: {
      extensions: ['.json', '.js'],
      modules: [root('src'), root('node_modules')],
      alias: { // FIX: velocity-react
        'lodash/object/omit': 'lodash/omit',
        'lodash/object/extend': 'lodash/extend',
        'lodash/lang/isObject': 'lodash/isObject',
        'lodash/lang/isEqual': 'lodash/isEqual',
        'lodash/collection/forEach': 'lodash/forEach',
        'lodash/collection/each': 'lodash/each',
        'lodash/collection/pluck': 'lodash/map',
        'lodash/object/keys': 'lodash/keys',
      },
    },
    output: {
      path: outputPath,
    },
    module: {
      strictExportPresence: true,
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: [
            { loader: 'babel-loader' },
          ],
        },
        cssLoader,
        scssLoader,
        {
          test: /\.(eot|svg|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
          use: [
            { loader: 'file-loader', options: { outputPath: 'fonts/', useRelativePath: true, name: '[name].[ext]' } },
          ],
        },
      ],
    },
    plugins: [
      new AssetsPlugin({
        path: outputPath,
        filename: 'webpack-assets.json',
        prettyPrint: true,
      }),
      /*
      * Plugin: CopyWebpackPlugin
      * Description: Copy files and directories in webpack.
      *
      * Copies project static assets.
      *
      * See: https://www.npmjs.com/package/copy-webpack-plugin
      */
      new CopyWebpackPlugin([
        { from: path.join(rootPath, 'assets') },
        { from: path.join(rootPath, 'locales'), to: 'locales' },
      ], {
        ignore: [
          'sass/**',
          'icons/**',
        ],
      }),
      new ExtractTextPlugin('css/[name].[chunkhash].css', { disable: false, allChunks: metadata.isProduction }),
      /*
      * Plugin: HtmlWebpackPlugin
      * Description: Simplifies creation of HTML files to serve your webpack bundles.
      * This is especially useful for webpack bundles that include a hash in the filename
      * which changes every compilation.
      *
      * See: https://github.com/ampedandwired/html-webpack-plugin
      */
      new HtmlWebpackPlugin({
        template: indexPath,
        title: metadata.title,
        chunksSortMode: 'dependency',
        inject: 'body',
      }),
      new ProgressPlugin({ profile: true, colors: true }),
      new webpack.DefinePlugin(definePluginOptions),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.$': 'jquery',
        'window.jQuery': 'jquery',
      }),
      /*
      * Plugin: CommonsChunkPlugin
      * Description: Shares common code between the pages.
      * It identifies common modules and put them into a commons chunk.
      *
      * See: https://webpack.github.io/docs/list-of-plugins.html#commonschunkplugin
      * See: https://github.com/webpack/docs/wiki/optimization#multi-page-app
      */
      new webpack.optimize.CommonsChunkPlugin({
        name: ['polyfills', 'vendor'].reverse(),
      }),
    ],
    node: {
      global: true,
      crypto: 'empty',
      process: true,
      module: false,
      clearImmediate: false,
      setImmediate: false,
    },
  };

  return config;
};

export default webpackConfig;
