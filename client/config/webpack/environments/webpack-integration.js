const makeConfig = () => {
  const config = {
    devtool: 'inline-source-map',
    cache: false,
    /*
    * Include polyfills or mocks for various node stuff
    * Description: Node configuration
    *
    * See: https://webpack.github.io/docs/configuration.html#node
    */
    node: {
      global: true,
      crypto: 'empty',
      fs: 'empty',
      process: false,
      module: false,
      clearImmediate: false,
      setImmediate: false,
    },
  };
  return config;
};

export default makeConfig;
