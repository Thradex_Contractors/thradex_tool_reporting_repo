const makeConfig = ({ serverHost, serverPort }) => {
  const config = {
    devtool: 'cheap-module-source-map',
    output: {
      filename: 'js/[name].bundle.js',
      sourceMapFilename: 'js/[name].bundle.map',
      chunkFilename: 'js/[id].chunk.js',
    },
    /**
     * Webpack Development Server configuration
     * Description: The webpack-dev-server is a little node.js Express server.
     * The server emits information about the compilation state to the client,
     * which reacts to those events.
     *
     * See: https://webpack.github.io/docs/webpack-dev-server.html
     */
    devServer: {
      port: serverPort,
      host: serverHost,
      historyApiFallback: true,
      watchOptions: {
        // if you're using Docker you may need this
        // aggregateTimeout: 300,
        // poll: 1000,
        ignored: /node_modules/,
      },
    },
  };
  return config;
};

export default makeConfig;
