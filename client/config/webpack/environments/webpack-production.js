import UglifyJSPlugin from 'uglifyjs-webpack-plugin';
import OptimizeCssAssetsPlugin from 'optimize-css-assets-webpack-plugin';

const makeConfig = () => {
  const config = {
    devtool: 'source-map',
    output: {
      filename: 'js/[name].[hash].bundle.js',
      sourceMapFilename: 'js/[name].[hash].map',
      chunkFilename: 'js/[id].[hash].chunk.js',
    },
    plugins: [
      new UglifyJSPlugin(),
      new OptimizeCssAssetsPlugin({
        cssProcessor: require('cssnano'), // eslint-disable-line
        cssProcessorOptions: { discardComments: { removeAll: true } },
        canPrint: true,
      }),
    ],
    /*
    * Include polyfills or mocks for various node stuff
    * Description: Node configuration
    *
    * See: https://webpack.github.io/docs/configuration.html#node
    */
    node: {
      global: true,
      crypto: 'empty',
      fs: 'empty',
      process: false,
      module: false,
      clearImmediate: false,
      setImmediate: false,
    },
  };
  return config;
};

export default makeConfig;
