﻿using System;

namespace Thradex.Models.Base
{
    public class BaseResponse
    {
        public string Identifier { get; set; }
        public int StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public object Result { get; set; }
        public DateTime RequestedOn { get; set; }
        public DateTime RespondedOn { get; set; }
    }
}