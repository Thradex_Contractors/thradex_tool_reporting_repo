﻿namespace Thradex.Models.Base
{
    public enum LevelType
    {
        CLIENT,
        OCH,
        ODU,
        OMS,
        OSC,
        OTS,
        OTU
    }

    public static class LevelTypeExtensions
    {
        public static string ToFriendlyString(this LevelType me)
        {
            switch (me)
            {
                case LevelType.CLIENT:
                    return "client";
                case LevelType.OCH:
                    return "och";
                case LevelType.ODU:
                    return "odu";
                case LevelType.OMS:
                    return "oms";
                case LevelType.OSC:
                    return "osc";
                case LevelType.OTS:
                    return "ots";
                case LevelType.OTU:
                    return "otu";
                default:
                    return me.ToString().ToLower();
            }
        }
    }
}
