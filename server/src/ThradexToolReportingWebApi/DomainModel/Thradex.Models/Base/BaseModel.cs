﻿namespace Thradex.Models.Base
{
    public class BaseModel
    {
        public string Name { get; set; }

        public LevelType LevelType { get; set; }

        protected string SetNullForEmpty(string input)
        {
            return string.IsNullOrEmpty(input) ? "NULL" : input;
        }
    }
}