﻿namespace Thradex.Models
{
    public class DocumentUploadResponse
    {
        public int RowsInserted { get; set; }
        public string FileName { get; set; }
    }
}