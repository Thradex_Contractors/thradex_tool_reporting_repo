﻿namespace Thradex.Models.Excels
{
    public class WdmSingleRouteOchOduModelResult
    {
        public string OpticalChannel { get; set; }
        public string OduHo { get; set; }
        public string OduLo { get; set; }
    }
}