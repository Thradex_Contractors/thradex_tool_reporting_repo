﻿using Thradex.Models.Base;
using System.Linq;

namespace Thradex.Models.Excels
{
    public class WdmSingleRouteDetailModelResult : BaseModel
    {
        public bool IsPositive { get; set; }
        public SourceSinkContextResult WdmTrailModel { get; set; }
        public WdmSingleRouteOchOduModelResult OchOduMapping { get; set; }

        public override string ToString()
        {
            var stringPropertieValues = new string[] {
                "id",
                Name,
                WdmTrailModel.Context,
                IsPositive ? "1" : "0",
                WdmTrailModel.NeName,
                WdmTrailModel.ShelfId.HasValue? WdmTrailModel.ShelfId.Value.ToString() : "NULL",
                WdmTrailModel.SlotId.HasValue? WdmTrailModel.SlotId.Value.ToString() : "NULL",
                WdmTrailModel.Board,
                WdmTrailModel.PortId.HasValue? WdmTrailModel.PortId.Value.ToString() : "NULL",
                OchOduMapping.OpticalChannel,
                OchOduMapping.OduHo,
                OchOduMapping.OduLo
            }.Select(v => SetNullForEmpty(v));
            return string.Join("\t", stringPropertieValues);
        }
    }
}