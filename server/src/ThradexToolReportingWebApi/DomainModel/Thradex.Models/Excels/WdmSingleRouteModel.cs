﻿using Thradex.Models.Base;
using System.Linq;

namespace Thradex.Models.Excels
{
    public class WdmSingleRouteModel : BaseModel
    {
        public string Level { get; set; }
        public string Direction { get; set; }
        public string Source { get; set; }
        public string SourceWavelength { get; set; }
        public string Sink { get; set; }
        public string SinkWavelength { get; set; }
        public string WorkingRoute { get; set; }
        public string ProtectionRoute { get; set; }
        public string BearerRateMbits { get; set; }
        public string Rate { get; set; }
        public string Customer { get; set; }

        public override string ToString()
        {
            var stringPropertieValues = new string[] {
                "id",
                Name,
                Level,
                Direction,
                Source,
                SourceWavelength,
                Sink,
                SinkWavelength,
                WorkingRoute,
                ProtectionRoute,
                BearerRateMbits,
                Rate,
                Customer,
            }.Select(v => SetNullForEmpty(v)); ;
            return string.Join("\t", stringPropertieValues);
        }
    }
}