﻿using Thradex.Models.Base;
using System.Linq;

namespace Thradex.Models.Excels
{
    public class ManageWdmTrailModel : BaseModel
    {
        public string ID { get; set; }
        public string Level { get; set; }
        public string Direction { get; set; }
        public string ServiceStatus { get; set; }
        public string AlarmStatus { get; set; }
        public string Source { get; set; }
        public string SourceWavelength { get; set; }
        public string Sink { get; set; }
        public string SinkWavelength { get; set; }
        public string BearerRateMbits { get; set; }
        public string Rate { get; set; }
        public string ServiceType { get; set; }
        public string Customer { get; set; }
        public string Creator { get; set; }
        public string CreatedTime { get; set; }
        public string ActivatedTime { get; set; }
        public string ConnectionStatus { get; set; }
        public string LockoutStatus { get; set; }
        public string Remarks { get; set; }
        public string OrderNo { get; set; }
        public string ServicioCiudad { get; set; }
        public string RutaProteccion { get; set; }
        public string ASONWDMTrail { get; set; }
        public string ServiceAlarmAnalysis { get; set; }
        public string PlatinumServiceGroupID { get; set; }
        public string PlatinumServiceGroupStatus { get; set; }
        public string ProtectionStatus { get; set; }
        public string UsageStatus { get; set; }
        public string OpticalCommissionStatus { get; set; }
        public string OSNRStatus { get; set; }
        public string MaintenanceStatus { get; set; }
        public string ODOptimizationStatus { get; set; }
        public string SuperChannel { get; set; }
        public string ModifiedTime { get; set; }
        public string ModifiedUser { get; set; }

        public string SITEchar
        {
            get
            {
                return $"{SRCSite}-{SNKSite}";
            }
        }

        public string SRCSite
        {
            get
            {
                return Source.Substring(0, Source.IndexOf("_"));
            }
        }

        public string SNKSite
        {
            get
            {
                return Sink.Substring(0, Sink.IndexOf("_"));
            }
        }

        public string SAMESite
        {
            get
            {
                return Source.Equals(Sink) ? "1" : "0";
            }
        }
        public override string ToString()
        {
            var stringPropertieValues = new string[] {
                //"id",
                ID,
                SITEchar,
                Level,
                Direction,
                ServiceStatus,
                AlarmStatus,
                Name,
                Source,
                SRCSite,
                SAMESite,
                SourceWavelength,
                Sink,
                SNKSite,
                SinkWavelength,
                BearerRateMbits,
                Rate,
                ServiceType,
                Customer,
                Creator,
                CreatedTime,
                ActivatedTime,
                ConnectionStatus,
                LockoutStatus,
                Remarks,
                OrderNo,
                ServicioCiudad,
                RutaProteccion,
                ASONWDMTrail,
                ServiceAlarmAnalysis,
                PlatinumServiceGroupID,
                PlatinumServiceGroupStatus,
                ProtectionStatus,
                UsageStatus,
                OpticalCommissionStatus,
                OSNRStatus,
                MaintenanceStatus,
                ODOptimizationStatus,
                SuperChannel,
                ModifiedTime,
                ModifiedUser,
             }.Select(v => SetNullForEmpty(v));
            return string.Join("\t", stringPropertieValues);
        }
    }
}