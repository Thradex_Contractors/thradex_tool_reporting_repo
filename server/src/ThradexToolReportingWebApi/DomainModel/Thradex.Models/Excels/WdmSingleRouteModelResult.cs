﻿using System.Collections.Generic;
using Thradex.Models.Base;

namespace Thradex.Models.Excels
{
    public class WdmSingleRouteModelResult : BaseModel
    {
        public WdmSingleRouteModelResult()
        {
            WorkingProtectionPositive = new List<WdmSingleRouteDetailModelResult>();
            WorkingProtectionNegative = new List<WdmSingleRouteDetailModelResult>();
        }

        public List<WdmSingleRouteDetailModelResult> WorkingProtectionPositive { get; set; }
        public List<WdmSingleRouteDetailModelResult> WorkingProtectionNegative { get; set; }
    }
}