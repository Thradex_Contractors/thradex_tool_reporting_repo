﻿using Thradex.Models.Base;
using System.Linq;
using System.Collections.Generic;

namespace Thradex.Models.Excels
{
    public class ManageWdmTrailModelResult : BaseModel
    {
        public ManageWdmTrailModelResult(LevelType level)
        {
            LevelType = level;
        }

        public SourceSinkContextResult Source { get; set; }
        public SourceSinkContextResult Sink { get; set; }

        // och, oms, osc, ots: no tiene OCHn y OTUn
        public override string ToString()
        {
            var stringPropertieValues = new List<string> {
                "id",
                Name,
                Source.Context,
                Source.NeName,
                Source.ShelfId.HasValue? Source.ShelfId.Value.ToString() : "NULL",
                Source.SlotId.HasValue? Source.SlotId.Value.ToString() : "NULL",
                Source.Board,
                Source.PortId.HasValue? Source.PortId.Value.ToString() : "NULL",
                Source.OCHn,
                Source.OTUn,
                Sink.Context,
                Sink.NeName,
                Sink.ShelfId.HasValue? Sink.ShelfId.Value.ToString() : "NULL",
                Sink.SlotId.HasValue? Sink.SlotId.Value.ToString() : "NULL",
                Sink.Board,
                Sink.PortId.HasValue? Sink.PortId.Value.ToString() : "NULL",
                Sink.OCHn,
                Sink.OTUn,
            };
            switch (LevelType)
            {
                case LevelType.OCH:
                case LevelType.OMS:
                case LevelType.OSC:
                case LevelType.OTS:
                    stringPropertieValues.RemoveRange(16, 2);
                    stringPropertieValues.RemoveRange(8, 2);
                    break;
                default:
                    break;
            }
            
            return string.Join("\t", stringPropertieValues.Select(value => SetNullForEmpty(value)));
        }
    }
}