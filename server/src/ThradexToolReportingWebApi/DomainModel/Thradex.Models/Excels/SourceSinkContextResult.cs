﻿namespace Thradex.Models.Excels
{
    public class SourceSinkContextResult
    {
        public string NeName { get; set; }
        public string Context { get; set; }
        public int? ShelfId { get; set; }
        public int? SlotId { get; set; }
        public string Board { get; set; }
        public int? PortId { get; set; }
        public string OCHn { get; set; }
        public string OTUn { get; set; }
    }
}