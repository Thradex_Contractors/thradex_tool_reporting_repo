﻿using ClosedXML.Excel;
using NUnit.Framework;
using System.Collections.Generic;
using Thradex.Framework.Tests.Utils;

namespace Thradex.Framework.Tests.ManageWdmTrail
{
    [TestFixture(Category = "ODU - Sink")]
    public class ManageWdmTrailODUSinkTest
    {
        private static IXLTable excelTable;
        private ManageWdmTrailParser manageWdmTrailParser;

        public static IEnumerable<TestCaseData> ManageWdmTrailTestCases
        {
            get
            {
                excelTable = ExcelHelpers.ReadExcelFile(ManageWdmTrail.ODUFilepath, "Sink");
                return ManageWdmTrail.GetTestCaseData(excelTable, "Sink", "ODU");
            }
        }

        [OneTimeSetUp]
        public void SetupContext()
        {
            manageWdmTrailParser = new ManageWdmTrailParser();
        }

        [OneTimeTearDown]
        public void CleanupContext()
        {
            excelTable.Dispose();
        }

        [TestCaseSource("ManageWdmTrailTestCases")]
        public void ValidateODUSourceTest(ManageWdnTrailTarget targetData)
        {
            var result = manageWdmTrailParser.Parse(targetData.Context);
            ManageWdmTrail.AssertRecord(result, targetData);
        }
    }
}