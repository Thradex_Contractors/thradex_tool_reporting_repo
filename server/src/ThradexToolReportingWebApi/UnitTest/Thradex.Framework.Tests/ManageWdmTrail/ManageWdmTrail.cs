﻿using ClosedXML.Excel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Thradex.Framework.Tests.Utils;
using Thradex.Models.Excels;

namespace Thradex.Framework.Tests.ManageWdmTrail
{
    public class ManageWdmTrail
    {
        public static string OTUFilepath = ExcelHelpers.GetFullFilePath("ManageWdmTrail/3.1.Manage_WDM_Trail_09-13-2017_16-07-15_TABLA_OTU.xlsx");

        public static string ODUFilepath = ExcelHelpers.GetFullFilePath("ManageWdmTrail/3.2.Manage_WDM_Trail_09-13-2017_16-07-15_TABLA_ODU.xlsx");

        public static string ClientFilepath = ExcelHelpers.GetFullFilePath("ManageWdmTrail/3.3.Manage_WDM_Trail_09-13-2017_16-07-15_TABLA_CLIENT.xlsx");

        private static List<ManageWdnTrailTarget> GetSourceRecords(IXLTable table, string context)
        {
            return ExcelHelpers.ParseExcelTable(table, row =>
                new ManageWdnTrailTarget()
                {
                    RowIndex = row.RowNumber(),
                    Context = row.GetCellValue(context),
                    NeName = row.GetCellValue("NE Name (largo) itr4 RESULTADO"),
                    ShelfId = row.GetNullableIntValue("Extraccion del shelf ID"),
                    SlotId = row.GetNullableIntValue("Slot ID"),
                    Board = row.GetCellValue("BOARD"),
                    PortId = row.GetNullableIntValue("Port ID")
                }
            );
        }

        public static IEnumerable<TestCaseData> GetTestCaseData(IXLTable excelTable, string context, string type)
        {
            var sourceRecords = GetSourceRecords(excelTable, context);
            var category = $"EXCEL DATA {type} {context}".ToUpper();
            var testCaseName = $"Validate{type}{context}Test";
            foreach (var source in sourceRecords)
            {
                var index = source.RowIndex.ToString().PadLeft(3, '0');
                yield return new TestCaseData(source)
                    // .SetCategory(category)
                    .SetName($"{index} - {testCaseName}");
            }
        }

        public static void AssertRecord(SourceSinkContextResult result, ManageWdnTrailTarget targetData)
        {
            Func<string, string> getMessage = fieldName => $"Invalid '{fieldName}' extracted from '{targetData.Context}' at '{targetData.RowIndex}' row index";

            Assert.That(result.Context, Is.Not.Empty);
            Assert.That(result.NeName, Is.EqualTo(targetData.NeName), getMessage("NeName"));
            Assert.That(result.ShelfId, Is.EqualTo(targetData.ShelfId), getMessage("ShelfId"));
            Assert.That(result.SlotId, Is.EqualTo(targetData.SlotId), getMessage("SlotId"));
            Assert.That(result.Board, Is.EqualTo(targetData.Board), getMessage("Board"));
            Assert.That(result.PortId, Is.EqualTo(targetData.PortId), getMessage("PortId"));
        }
    }
}