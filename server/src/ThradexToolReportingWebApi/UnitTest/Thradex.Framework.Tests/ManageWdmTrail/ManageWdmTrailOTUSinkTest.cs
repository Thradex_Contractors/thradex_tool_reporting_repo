﻿using ClosedXML.Excel;
using NUnit.Framework;
using System.Collections.Generic;
using Thradex.Framework.Tests.Utils;

namespace Thradex.Framework.Tests.ManageWdmTrail
{
    [TestFixture(Category = "OTU - Sink")]
    public class ManageWdmTrailOTUSinkTest
    {
        private static IXLTable excelTable;
        private ManageWdmTrailParser manageWdmTrailParser;

        public static IEnumerable<TestCaseData> ManageWdmTrailTestCases
        {
            get
            {
                excelTable = ExcelHelpers.ReadExcelFile(ManageWdmTrail.OTUFilepath, "Sink");
                return ManageWdmTrail.GetTestCaseData(excelTable, "Sink", "OTU");
            }
        }

        [OneTimeSetUp]
        public void SetupContext()
        {
            manageWdmTrailParser = new ManageWdmTrailParser();
        }

        [OneTimeTearDown]
        public void CleanupContext()
        {
            excelTable.Dispose();
        }

        //[TestCaseSource("ManageWdmTrailTestCases")]
        public void ValidateODUSourceTest(ManageWdnTrailTarget targetData)
        {
            var result = manageWdmTrailParser.Parse(targetData.Context);
            ManageWdmTrail.AssertRecord(result, targetData);
        }

        [Category("Individual Cases")]
        [TestCase("WA2_6800_808F-WA2_6800_808F-shelf3-1-M40V-11(M10)&MIR_6800_611F-MIR_6800_611F_MIRAFLORES(M)-shelf3-8-13LSX-1(OUT)", "MIR_6", "MIR_6800_611F_MIRAFLORES(M)-shelf3-8-13LSX-1(OUT)")]
        [TestCase("WA2_6800_808F-WA2_6800_808F-shelf1-1-M40V-11(M10)&SIS_6800_609F-SIS_6800_609F_603E_SAN ISIDRO(M)-shelf4-10-13LSX-1(OUT)", "SIS_6", "SIS_6800_609F_603E_SAN ISIDRO(M)-shelf4-10-13LSX-1(OUT)")]
        [TestCase("AYA_9800U16_405F-1_AYACUCHO(M)-shelf0-6-U3U401-1(IN1/OUT1 (ALIEN CLARO AYA-AHY))-OCh:1-OTU4", "OCh:1", "OTU4")]
        [TestCase("MON_8800T32_603E-1_MONTERRICO(M)-shelf0-36-57NS4-1(IN/OUT)-OCh:1-OTU4&RCY_8800T32_102F-1_RECUAY(M)-shelf0-19-57NS4-1(IN/OUT)-OCh:1-OTU4", "OCh:1", "OTU4&RCY_8800T32_102F-1_RECUAY(M)-shelf0-19-57NS4-1(IN/OUT)-OCh:1-OTU4")]
        [TestCase("NX5_1800V_101F-4_NEXTEL - SBO-shelf0-13-F5HUNQ2-4(OUT4)-OCh:1-OTU2", "OCh:1", "OTU2")]
        [TestCase("TR3_6800_603E-TR3_6800_603E_TARAPOTO PISO3(M)-shelf2-1-12LSX-1(IN/OUT)", "", "")]
        [TestCase("ETL_8800T32_112E-1_ET LURIN(M)-shelf0-29-55TQX-4(TX2)-1", "1", "")]
        public void ValidateExtractedNeNameFromSourceTest(string source, string ochn, string otun)
        {
            var result = manageWdmTrailParser.Parse(source);

            Assert.That(result.OCHn, Is.EqualTo(ochn), $"Invalid extracted value from '{source}'");
            Assert.That(result.OTUn, Is.EqualTo(otun), $"Invalid extracted value from '{source}'");
        }
    }
}