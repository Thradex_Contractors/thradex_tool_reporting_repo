﻿using ClosedXML.Excel;
using NUnit.Framework;
using System.Collections.Generic;
using Thradex.Framework.Tests.Utils;

namespace Thradex.Framework.Tests.ManageWdmTrail
{
    [TestFixture(Category = "Client - Source")]
    public class ManageWdmTrailClientSourceTest
    {
        private static IXLTable excelTable;
        private ManageWdmTrailParser manageWdmTrailParser;

        public static IEnumerable<TestCaseData> ManageWdmTrailTestCases
        {
            get
            {
                excelTable = ExcelHelpers.ReadExcelFile(ManageWdmTrail.ClientFilepath, "Source");
                return ManageWdmTrail.GetTestCaseData(excelTable, "Source", "Client");
            }
        }

        [OneTimeSetUp]
        public void SetupContext()
        {
            manageWdmTrailParser = new ManageWdmTrailParser();
        }

        [OneTimeTearDown]
        public void CleanupContext()
        {
            excelTable.Dispose();
        }

        [TestCaseSource("ManageWdmTrailTestCases")]
        public void ValidateODUSourceTest(ManageWdnTrailTarget targetData)
        {
            var result = manageWdmTrailParser.Parse(targetData.Context);
            ManageWdmTrail.AssertRecord(result, targetData);
        }
    }
}