﻿using Thradex.Models.Excels;

namespace Thradex.Framework.Tests.Utils
{
    public class SingleRouteSpecificTarget : WdmSingleRouteDetailModelResult
    {
        public int RowIndex { get; set; }
        public string Context { get; set; }
    }
}