﻿using ClosedXML.Excel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Thradex.Framework.Tests.Utils
{
    public static class ExcelHelpers
    {
        public static string GetFullFilePath(string filePath)
        {
            return Path.Combine(TestContext.CurrentContext.TestDirectory, $@"Data\{filePath}");
        }

        public static IXLTable ReadExcelFile(string filePath, string sheetName)
        {
            var workbook = new XLWorkbook(filePath);
            var ws = workbook.Worksheet(sheetName);
            var firstPossibleAddress = ws.FirstCellUsed().Address;
            var lastPossibleAddress = ws.LastCellUsed().Address;
            return ws.Range(firstPossibleAddress, lastPossibleAddress).AsTable();
        }

        public static string GetCellValue(this IXLTableRow row, string header)
        {
            var cell = row.Field(header);
            try
            {
                if (!string.IsNullOrEmpty(cell.ValueCached) && cell.ValueCached.Contains("#VALUE"))
                    return null;

                if (!string.IsNullOrEmpty(cell.ValueCached) && cell.Value.ToString() != cell.ValueCached)
                    return cell.ValueCached;

                return cell.Value.ToString();
            }
            catch (Exception ex)
            {
                return cell.ValueCached;
            }
        }

        public static int? GetNullableIntValue(this IXLTableRow row, string header)
        {
            var value = GetCellValue(row, header);
            var i = 0;
            return (Int32.TryParse(value, out i) ? i : (int?)null);
        }

        public static List<TModel> ParseExcelTable<TModel>(IXLTable excelTable, Func<IXLTableRow, TModel> parseRowFunc)
        {
            return excelTable
                .DataRange
                .Rows()
                .Aggregate(
                    new List<TModel>(),
                    (acc, row) =>
                    {
                        var parsedData = parseRowFunc(row);
                        acc.Add(parsedData);
                        return acc;
                    });
        }
    }
}