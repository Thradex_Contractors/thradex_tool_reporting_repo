﻿using Thradex.Models.Excels;

namespace Thradex.Framework.Tests.Utils
{
    public class ManageWdnTrailTarget : SourceSinkContextResult
    {
        public int RowIndex { get; set; }
    }
}