﻿using ClosedXML.Excel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Thradex.Framework.Tests.Utils;
using Thradex.Models.Excels;

namespace Thradex.Framework.Tests.SingleRoutes
{
    //public class SingleRouteSpecifics
    //{
    //    public static string ODUPositiveFilepath = ExcelHelpers.GetFullFilePath("SingleRoutes/4.2.1.1.Single_Route_Specifics_09-13-2017_16-43-52_TABLA_ODU_breakdown_W-Postive_PARSE.2.xlsx");

    //    private static List<SingleRouteSpecificTarget> GetSourceRecords(IXLTable table, string context)
    //    {
    //        return ExcelHelpers.ParseExcelTable(table, row =>
    //            new SingleRouteSpecificTarget()
    //            {
    //                RowIndex = row.RowNumber(),
    //                Context = row.GetCellValue(context).Replace("\n", ""),
    //                WdmTrailModel = new ManageWdmTrailModelResult
    //                {
    //                    NeName = row.GetCellValue("NE Name (largo) itr4 RESULTADO").Replace("\n", ""),
    //                    ShelfId = row.GetNullableIntValue("Extraccion del shelf ID"),
    //                    SlotId = row.GetNullableIntValue("Slot ID"),
    //                    Board = row.GetCellValue("BOARD"),
    //                    PortId = row.GetNullableIntValue("Port ID")
    //                },
    //                OchOduMapping = new WdmSingleRouteOchOduModelResult
    //                {
    //                    OpticalChannel = row.GetCellValue("Optical Channel (OCh)"),
    //                    OduHo = row.GetCellValue("string ODU HO"),
    //                    OduLo = row.GetCellValue("string ODU LO")
    //                }
    //            }
    //        );
    //    }

    //    public static IEnumerable<TestCaseData> GetTestCaseData(IXLTable excelTable, string context, string type)
    //    {
    //        var sourceRecords = GetSourceRecords(excelTable, context);
    //        var category = $"EXCEL DATA {type} Single Route Specific".ToUpper();
    //        var testCaseName = $"Validate{type}SingleRouteSpeficTest";
    //        foreach (var source in sourceRecords)
    //        {
    //            var index = source.RowIndex.ToString().PadLeft(3, '0');
    //            yield return new TestCaseData(source)
    //                // .SetCategory(category)
    //                .SetName($"{index} - {testCaseName}");
    //        }
    //    }

    //    public static string GenerateContext(string source, bool isPositive)
    //    {
    //        var trail = isPositive ? "Positive" : "Negative";
    //        return $"{trail}: {source}";
    //    }

    //    public static void AssertRecord(WdmSingleRouteDetailModelResult result, SingleRouteSpecificTarget targetData)
    //    {
    //        Func<string, string> getMessage = fieldName => $"Invalid '{fieldName}' extracted from '{targetData.Context}' at '{targetData.RowIndex}' row index";
            
    //        Assert.That(result.WdmTrailModel.NeName, Is.EqualTo(targetData.WdmTrailModel.NeName), getMessage("NeName"));
    //        Assert.That(result.WdmTrailModel.ShelfId, Is.EqualTo(targetData.WdmTrailModel.ShelfId), getMessage("ShelfId"));
    //        Assert.That(result.WdmTrailModel.SlotId, Is.EqualTo(targetData.WdmTrailModel.SlotId), getMessage("SlotId"));
    //        Assert.That(result.WdmTrailModel.Board, Is.EqualTo(targetData.WdmTrailModel.Board), getMessage("Board"));
    //        Assert.That(result.WdmTrailModel.PortId, Is.EqualTo(targetData.WdmTrailModel.PortId), getMessage("PortId"));

    //        Assert.That(result.OchOduMapping.OpticalChannel, Is.EqualTo(targetData.OchOduMapping.OpticalChannel), getMessage("OpticalChannel"));
    //        Assert.That(result.OchOduMapping.OduHo, Is.EqualTo(targetData.OchOduMapping.OduHo), getMessage("OduHo"));
    //        Assert.That(result.OchOduMapping.OduLo, Is.EqualTo(targetData.OchOduMapping.OduLo), getMessage("OduLo"));
    //    }
    //}
}