﻿using NUnit.Framework;

namespace Thradex.Framework.Tests.SingleRoutes
{
    //[TestFixture(Category = "SingleRoute Breakdown")]
    //public class SingleRouteBreakdownTest
    //{
    //    private SingleRouteSpecificsParser singleRouteSpecificsParser;

    //    [OneTimeSetUp]
    //    public void SetupContext()
    //    {
    //        var manageWdmTrailParser = new ManageWdmTrailParser();
    //        var singleRouteSpecificsOchOduMappingParser = new SingleRouteSpecificsOchOduMappingParser();
    //        singleRouteSpecificsParser = new SingleRouteSpecificsParser(manageWdmTrailParser, singleRouteSpecificsOchOduMappingParser);
    //    }

    //    [TestCase]
    //    public void ValidateSingleRouteBreakdownTest()
    //    {
    //        var context = @"Positive:
    //            ICA_9800U32_907F-1_ICA(M)-shelf0-1-V2T220-1(RX1/TX1 - CORE BA ICA_ARE_1)-1->
    //            ICA_9800U32_907F-1_ICA(M)-shelf0-7-U3U401-1(IN1/OUT1)-OCh:1-ODU4:1-ODU2:1->
    //            ARE_9800U32_604E-shelf0-8-U3U401-1(IN1/OUT1)-OCh:1-ODU4:1-ODU2:1->
    //            ARE_9800U32_604E-shelf0-32-V2T220-1(RX1/TX1 - CORE BA ICA_ARE_1)-1
    //            Negative:
    //            ARE_9800U32_604E-shelf0-32-V2T220-1(RX1/TX1 - CORE BA ICA_ARE_1)-1->
    //            ARE_9800U32_604E-shelf0-8-U3U401-1(IN1/OUT1)-OCh:1-ODU4:1-ODU2:1->
    //            ICA_9800U32_907F-1_ICA(M)-shelf0-7-U3U401-1(IN1/OUT1)-OCh:1-ODU4:1-ODU2:1->
    //            ICA_9800U32_907F-1_ICA(M)-shelf0-1-V2T220-1(RX1/TX1 - CORE BA ICA_ARE_1)-1";

    //        var result = singleRouteSpecificsParser.Parse(context);
    //        Assert.That(result.WorkingProtectionPositive.Count, Is.EqualTo(4));
    //        Assert.That(result.WorkingProtectionNegative.Count, Is.EqualTo(4));

    //        Assert.That(result.WorkingProtectionPositive[1].OchOduMapping.OpticalChannel, Is.EqualTo("OCh:1"));
    //        Assert.That(result.WorkingProtectionPositive[1].OchOduMapping.OduHo, Is.EqualTo("ODU4:1"));
    //        Assert.That(result.WorkingProtectionPositive[1].OchOduMapping.OduLo, Is.EqualTo("ODU2:1"));

    //        Assert.That(result.WorkingProtectionNegative[0].OchOduMapping.OpticalChannel, Is.Null);
    //        Assert.That(result.WorkingProtectionNegative[0].OchOduMapping.OduHo, Is.Null);
    //        Assert.That(result.WorkingProtectionNegative[0].OchOduMapping.OduLo, Is.Null);
    //    }
    //}
}
