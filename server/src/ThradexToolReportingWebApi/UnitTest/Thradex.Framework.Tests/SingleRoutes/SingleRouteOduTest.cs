﻿using ClosedXML.Excel;
using NUnit.Framework;
using System.Collections.Generic;
using Thradex.Framework.Tests.Utils;
using System.Linq;

namespace Thradex.Framework.Tests.SingleRoutes
{
    //[TestFixture(Category = "ODU - SingleRoute")]
    //public class SingleRouteOduTest
    //{
    //    private static IXLTable excelTable;
    //    private SingleRouteSpecificsParser singleRouteSpecificsParser;

    //    public static IEnumerable<TestCaseData> SingleRouteSpecificTestCases
    //    {
    //        get
    //        {
    //            excelTable = ExcelHelpers.ReadExcelFile(SingleRouteSpecifics.ODUPositiveFilepath, "WORKING Positive");
    //            return SingleRouteSpecifics.GetTestCaseData(excelTable, "Source", "ODU");
    //        }
    //    }

    //    [OneTimeSetUp]
    //    public void SetupContext()
    //    {
    //        var manageWdmTrailParser = new ManageWdmTrailParser();
    //        var singleRouteSpecificsOchOduMappingParser = new SingleRouteSpecificsOchOduMappingParser();
    //        singleRouteSpecificsParser = new SingleRouteSpecificsParser(manageWdmTrailParser, singleRouteSpecificsOchOduMappingParser);
    //    }

    //    [OneTimeTearDown]
    //    public void CleanupContext()
    //    {
    //        excelTable.Dispose();
    //    }

    //    [TestCaseSource("SingleRouteSpecificTestCases")]
    //    public void ValidateODUSourceTest(SingleRouteSpecificTarget targetData)
    //    {
    //        var result = singleRouteSpecificsParser.Parse(SingleRouteSpecifics.GenerateContext(targetData.Context, true));
    //        SingleRouteSpecifics.AssertRecord(result.WorkingProtectionPositive[0], targetData);
    //    }
    //}
}
