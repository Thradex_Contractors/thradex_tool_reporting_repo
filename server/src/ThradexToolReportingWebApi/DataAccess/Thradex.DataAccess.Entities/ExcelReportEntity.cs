﻿using System;

namespace Thradex.DataAccess.Entities
{
    public class ExcelReportEntity
    {
        public int ExcelReportId { set; get; }
        public string FullPath { set; get; }
        public DateTime IssuedOn { set; get; }
        public bool IsActive { set; get; }
    }
}