﻿namespace Thradex.DataAccess.Entities
{
    public class NEReport
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string IPAddress { get; set; }
        public string MACAddress { get; set; }
        public string ID { get; set; }
        public string SoftwareVersion { get; set; }
        public string PhysicalLocation { get; set; }
        public string CreateTime { get; set; }
        public string FiberCableCount { get; set; }
        public string RunningStatus { get; set; }
        public string Subnet { get; set; }
        public string SubnetPath { get; set; }
        public string Alias { get; set; }
        public string Remarks { get; set; }
        public string PatchVersionList { get; set; }
        public string CustomizedColumn { get; set; }
        public string LSRID { get; set; }
        public string MaintenanceStatus { get; set; }
        public string GatewayType { get; set; }
        public string Gateway { get; set; }
        public string Optical { get; set; }
        public string SubrackType { get; set; }
        public string ConferenceCall { get; set; }
        public string OrderwirePhone { get; set; }
        public string Subtype { get; set; }
    }
}