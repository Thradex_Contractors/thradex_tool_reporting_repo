﻿using MySql.Data.MySqlClient;
using Thradex.DataAccess.Database.Interfaces;
using Thradex.SettingsManager.Interfaces;

namespace Thradex.DataAccess.Database
{
    public class MySqlConnectionDatabase : ISqlConnectionDatabase
    {
        private readonly IDatabaseSettings _databaseSettings;

        public MySqlConnectionDatabase(IDatabaseSettings databaseSettings)
        {
            _databaseSettings = databaseSettings;
        }

        public string Ping()
        {
            try
            {
                using (var con = new MySqlConnection(_databaseSettings.ConnectionString))
                {
                    con.Open();
                    return "Connected";
                }
            }
            catch (MySqlException ex)
            {
                var connectionString = _databaseSettings.ConnectionString;
                return ex.Message + connectionString + ex.StackTrace;
            }
        }
    }
}