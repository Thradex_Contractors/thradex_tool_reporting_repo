﻿using ClosedXML.Excel;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Thradex.SettingsManager.Interfaces;

namespace Thradex.DataAccess.Database
{
    public abstract class BaseReportDatabase
    {
        protected readonly IDatabaseSettings _databaseSettings;

        public BaseReportDatabase(IDatabaseSettings databaseSettings)
        {
            _databaseSettings = databaseSettings;
        }

        protected void GenerateTSV<TModel>(string tsvFilePath, IList<TModel> modelList)
        {
            using (StreamWriter wr = new StreamWriter(tsvFilePath))
            {
                modelList.ForEach(row => wr.WriteLine(row.ToString()));
            }
        }

        protected IXLTable ReadExcelFile(string fullPath)
        {
            //1. Read the excel file
            var workbook = new XLWorkbook(fullPath);
            var ws = workbook.Worksheets.First();
            var firstPossibleAddress = ws.FirstCellUsed().Address;
            var lastPossibleAddress = ws.LastCellUsed().Address;
            var table = ws.Range(firstPossibleAddress, lastPossibleAddress).AsTable();
            return table;
        }

        protected int BulkLoader(string table, string tsvFilePath)
        {
            using (var con = new MySqlConnection(_databaseSettings.ConnectionString))
            {
                con.Open();
                var bl = new MySqlBulkLoader(con)
                {
                    TableName = table,
                    FieldTerminator = "\t",
                    LineTerminator = "\r\n",
                    FileName = tsvFilePath,
                    NumberOfLinesToSkip = 0,
                    FieldQuotationCharacter = '"',
                    FieldQuotationOptional= false
                };
                var inserted = bl.Load();
                return inserted;
            }
        }
    }
}