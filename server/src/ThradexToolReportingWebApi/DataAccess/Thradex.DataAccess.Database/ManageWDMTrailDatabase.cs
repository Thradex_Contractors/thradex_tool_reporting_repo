﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;
using Thradex.DataAccess.Database.Interfaces;
using Thradex.SettingsManager.Interfaces;
using Dapper;
using System.Linq;

namespace Thradex.DataAccess.Database
{
    public class ManageWdmTrailDatabase : BaseReportDatabase, IExcelToDatabase
    {
        public ManageWdmTrailDatabase(IDatabaseSettings databaseSettings) : base(databaseSettings)
        {
        }

        public List<TModel> Get<TModel>(string tableName) where TModel : class, new()
        {
            using (var con = new MySqlConnection(_databaseSettings.ConnectionString))
            {
                var sql = $"SELECT * FROM {tableName}";
                con.Open();
                var result = con.Query<TModel>(sql).ToList< TModel>();
                return result;
            }
        }

        public int Insert<TModel>(string tableName, string tsvFilePath, IList<TModel> modelList)
        {
            this.GenerateTSV(tsvFilePath, modelList);
            return this.BulkLoader(tableName, tsvFilePath);
        }
    }
}