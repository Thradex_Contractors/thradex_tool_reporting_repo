﻿namespace Thradex.DataAccess.Database.Interfaces
{
    public interface ISqlConnectionDatabase
    {
        string Ping();
    }
}