﻿using System.Collections.Generic;

namespace Thradex.DataAccess.Database.Interfaces
{
    public interface IExcelToDatabase 
    {
        int Insert<TModel>(string tableName, string tsvFilePath, IList<TModel> modelList);

        List<TModel> Get<TModel>(string tableName) where TModel : class, new();
    }
}