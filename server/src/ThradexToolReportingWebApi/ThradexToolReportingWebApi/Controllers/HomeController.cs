﻿using System.Web.Http;
using Thradex.Services.Interfaces;

namespace ThradexToolReportingWebApi.Controllers
{
    [RoutePrefix("api/home")]
    public class HomeController : ApiController
    {
        private readonly ISqlConnectionService _sqlConnectionService;

        public HomeController(ISqlConnectionService sqlConnectionService)
        {
            _sqlConnectionService = sqlConnectionService;
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok("This is your Home page");
        }

        [Route("ping")]
        [HttpGet]
        public IHttpActionResult Ping()
        {
            var result = _sqlConnectionService.Ping();
            return Ok(result);
        }
    }
}