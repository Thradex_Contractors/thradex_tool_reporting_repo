﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Thradex.Services.Interfaces.ProcessExcel;
using Thradex.SettingsManager.Interfaces;
using ThradexToolReportingWebApi.Base;
using ThradexToolReportingWebApi.RoutePrefix;

namespace ThradexToolReportingWebApi.Controllers.v1
{
    [ApiVersion1RoutePrefix("documents")]
    public class DocumentController : BaseApiController
    {
        private readonly IGeneralSettings _generalSettings;
        private readonly IProcessExcelStrategyService _processExcelStrategyService;

        public DocumentController(IGeneralSettings generalSettings, IProcessExcelStrategyService processExcelStrategyService)
        {
            _generalSettings = generalSettings;
            _processExcelStrategyService = processExcelStrategyService;
        }

        [Route("upload/{fileIdentifier}")]
        [HttpPost]
        public async Task<IHttpActionResult> Upload(string fileIdentifier)
        {
            try
            {
                var fileName = fileIdentifier == "file3" ? "ManageWdmTrail" : "SingleRoutes";
                await _processExcelStrategyService.Execute(fileIdentifier, fileName);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}