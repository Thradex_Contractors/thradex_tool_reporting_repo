﻿using Microsoft.Owin;
using Newtonsoft.Json.Serialization;
using Owin;
using SimpleInjector.Integration.WebApi;
using System.Net.Http.Formatting;
using System.Web.Http;
using ThradexToolReportingWebApi.PolicyAttributes;

[assembly: OwinStartup(typeof(ThradexToolReportingWebApi.App_Start.StartUp))]

namespace ThradexToolReportingWebApi.App_Start
{
    public class StartUp
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            config.MapHttpAttributeRoutes();

            //To discuss about Location
            // Web API configuration and services
            //CORS
            InitializerCors(config);

            // Formatters
            InitializeFormatters(config);

            //SimpleInjector
            InitializeDependencyResolver(config);

            //User HTTPS
            //InitializeForceHttpsHandler(config);

            app.UseWebApi(config);
        }

        private static void InitializerCors(HttpConfiguration config)
        {
            config.SetCorsPolicyProviderFactory(new ThradexCorsPolicyFactory());
            config.EnableCors();
        }

        private static void InitializeFormatters(HttpConfiguration config)
        {
            var formatters = config.Formatters;
            formatters.Clear();
            formatters.Add(new JsonMediaTypeFormatter());
            formatters.JsonFormatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
            formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            formatters.JsonFormatter.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            formatters.JsonFormatter.SerializerSettings.DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat;
            formatters.JsonFormatter.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Local;
        }

        private static void InitializeDependencyResolver(HttpConfiguration config)
        {
            Thradex.Ioc.StartUp.RegisterAllComponents();
            config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(Thradex.Ioc.StartUp.container);
        }
    }
}