﻿using System.Net.Http;
using System.Web.Http.Cors;

namespace ThradexToolReportingWebApi.PolicyAttributes
{
    public class ThradexCorsPolicyFactory : ICorsPolicyProviderFactory
    {
        private ICorsPolicyProvider _provider = new ThradexCorsPolicyAttribute();

        public ICorsPolicyProvider GetCorsPolicyProvider(HttpRequestMessage request)
        {
            return _provider;
        }
    }
}