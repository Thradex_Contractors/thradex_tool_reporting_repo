﻿using ClosedXML.Excel;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Thradex.DataAccess.Database.Interfaces;

namespace Thradex.Services.Base
{
    public abstract class BaseDocumentService
    {
        protected readonly IExcelToDatabase _excelToDatabase;

        public BaseDocumentService(IExcelToDatabase excelToDatabase)
        {
            _excelToDatabase = excelToDatabase;
        }

        protected IXLTable ReadExcelFile(string fullPath)
        {
            //1. Read the excel file
            var workbook = new XLWorkbook(fullPath);
            var ws = workbook.Worksheets.First();
            var firstPossibleAddress = ws.FirstCellUsed().Address;
            var lastPossibleAddress = ws.LastCellUsed().Address;
            var table = ws.Range(firstPossibleAddress, lastPossibleAddress).AsTable();
            return table;
        }

        protected void InsertDatabase<TModel>(string tableName, string filePath, string suffix, List<TModel> excelTableOTUSource)
        {
            var fileNameWithOutExt = $"{Path.GetDirectoryName(filePath)}{@"\"}{Path.GetFileNameWithoutExtension(filePath)}";
            var tsvFilePath = $"{fileNameWithOutExt}_{suffix}.tsv";
            var inserted = _excelToDatabase.Insert(tableName, tsvFilePath, excelTableOTUSource);
        }
    }
}