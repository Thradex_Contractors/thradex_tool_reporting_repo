﻿using System.Threading.Tasks;
using Thradex.Services.Interfaces.ProcessExcel;

namespace Thradex.Services.ProcessExcel
{
    public class ProcessExcelStrategyService : IProcessExcelStrategyService
    {
        private readonly IProcessExcelResolver _processExcelResolver;

        public ProcessExcelStrategyService(IProcessExcelResolver processExcelResolver)
        {
            _processExcelResolver = processExcelResolver;
        }

        public async Task Execute(string fileIdentifier, string fileName)
        {
            var documentService = _processExcelResolver.Resolve(fileIdentifier);
            await documentService.Process(fileName);
        }
    }
}