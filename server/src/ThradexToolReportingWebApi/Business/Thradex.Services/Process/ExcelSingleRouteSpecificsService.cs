﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Thradex.DataAccess.Database.Interfaces;
using Thradex.Framework.Interfaces;
using Thradex.Models;
using Thradex.Models.Excels;
using Thradex.Services.Base;
using Thradex.Services.Extensions;
using Thradex.Services.Interfaces.ProcessExcel;
using Thradex.SettingsManager.Interfaces;

namespace Thradex.Services.ProcessExcel
{
    public class ExcelSingleRouteSpecificsService : BaseDocumentService, IProcessExcelService
    {
        private readonly IGeneralSettings _generalSettings;
        private readonly IGeneralParser<WdmSingleRouteModelResult> _parserSingleRoute;

        public ExcelSingleRouteSpecificsService(IExcelToDatabase excelToDatabase, IGeneralSettings generalSettings, IGeneralParser<WdmSingleRouteModelResult> parserSingleRoute)
            : base(excelToDatabase)
        {
            _generalSettings = generalSettings;
            _parserSingleRoute = parserSingleRoute;
        }

        public string FileIdentifier => "File4";

        public async Task<DocumentUploadResponse> Process(string fileName)
        {
            //Save the file in shared folder
            var path = string.Format(@"{0}\{1}", _generalSettings.TargetFolderPath, fileName);
            //File.WriteAllBytes(path, bytes);

            var excelTable = base.ReadExcelFile(path);

            var excelModelList = excelTable.ToListOf<WdmSingleRouteModel>();

            var tasks = new List<Task>
            {
                Task.Run(() => ProcessBaseRecords(excelModelList, model => model.Level.Contains("ODU"), "odu", path))
            };

            await Task.WhenAll(tasks);

            var response = new DocumentUploadResponse()
            {
                RowsInserted = 0,
                FileName = Path.GetFileName(path)
            };
            return await Task.FromResult(response);
        }

        private void ProcessBaseRecords(List<WdmSingleRouteModel> excelModelList, Func<WdmSingleRouteModel, bool> criteria, string level, string filePath)
        {
            var excelBaseTable = excelModelList.Where(criteria).ToList();
            var tableName = $"excel_singleroute_{level}";
            InsertDatabase(tableName, filePath, tableName, excelBaseTable);
            ProcessLevelRecords(excelBaseTable, level, "working", filePath);
            ProcessLevelRecords(excelBaseTable, level, "protection", filePath);
        }

        private void ProcessLevelRecords(List<WdmSingleRouteModel> excelLevelRecords, string level, string context, string filePath)
        {
            var excelTrailTable = ParseTrails(excelLevelRecords, context.Equals("working"));

            //After parse4, we have to parse3 for each positive & negative values
            var resultPositive = ParsePositiveNegativeTrails(excelTrailTable, true);
            InsertDatabase($"excel_singleroute_{level}_{context}", filePath, $"singleroute_{level}_{context}_positive", resultPositive);

            var resultNegative = ParsePositiveNegativeTrails(excelTrailTable, false);
            InsertDatabase($"excel_singleroute_{level}_{context}", filePath, $"singleroute_{level}_{context}_negative", resultNegative);
        }

        private List<WdmSingleRouteModelResult> ParseTrails(List<WdmSingleRouteModel> excelModelList, bool useWorkingRoute)
        {
            return excelModelList
                .Aggregate(
                    new List<WdmSingleRouteModelResult>(),
                    (acc, item) =>
                    {
                        var model = _parserSingleRoute.Parse(useWorkingRoute ? item.WorkingRoute : item.ProtectionRoute);
                        model.Name = item.Name;
                        acc.Add(model);
                        return acc;
                    });
        }

        private List<WdmSingleRouteDetailModelResult> ParsePositiveNegativeTrails(List<WdmSingleRouteModelResult> list, bool isPositive)
        {
            return list.Aggregate(
                new List<WdmSingleRouteDetailModelResult>(),
                (acc, item) =>
                {
                    var trails = isPositive ? item.WorkingProtectionPositive : item.WorkingProtectionNegative;
                    trails.ForEach(wpp =>
                    {
                        wpp.Name = item.Name;
                        acc.Add(wpp);
                    });
                    return acc;
                });
        }
    }
}