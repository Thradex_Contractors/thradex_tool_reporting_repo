﻿using System.Collections.Generic;
using System.Linq;
using Thradex.Services.Interfaces.ProcessExcel;

namespace Thradex.Services.ProcessExcel
{
    public class ProcessExcelResolver : IProcessExcelResolver
    {
        private readonly IEnumerable<IProcessExcelService> _documentServices;

        public ProcessExcelResolver(IEnumerable<IProcessExcelService> documentServices)
        {
            _documentServices = documentServices;
        }

        public IProcessExcelService Resolve(string fileIdentifier)
        {
            var result = _documentServices.FirstOrDefault(method => method.FileIdentifier.ToLower().Equals(fileIdentifier));
            return result;
        }
    }
}