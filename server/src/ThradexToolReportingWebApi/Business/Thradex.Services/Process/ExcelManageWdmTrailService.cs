﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Thradex.DataAccess.Database.Interfaces;
using Thradex.Framework.Interfaces;
using Thradex.Models;
using Thradex.Models.Base;
using Thradex.Models.Excels;
using Thradex.Services.Base;
using Thradex.Services.Interfaces.ProcessExcel;
using Thradex.SettingsManager.Interfaces;

namespace Thradex.Services.ProcessExcel
{
    public class ExcelManageWdmTrailService : BaseDocumentService, IProcessExcelService
    {
        private readonly IGeneralSettings _generalSettings;
        private readonly IGeneralParser<SourceSinkContextResult> _manageWdmTrailParser;

        public string FileIdentifier => "File3";

        public ExcelManageWdmTrailService(IExcelToDatabase excelToDatabase, IGeneralSettings generalSettings, IGeneralParser<SourceSinkContextResult> manageWdmTrailParser)
            : base(excelToDatabase)
        {
            _generalSettings = generalSettings;
            _manageWdmTrailParser = manageWdmTrailParser;
        }

        public async Task<DocumentUploadResponse> Process(string fileName)
        {
            //Save the file in shared folder
            var path = string.Format(@"{0}\{1}", _generalSettings.TargetFolderPath, fileName);
            //File.WriteAllBytes(path, bytes);

            //var excelTable = base.ReadExcelFile(path);

            var excelModelList = _excelToDatabase.Get<ManageWdmTrailModel>("excel_manageWdmTrail");
            //var excelModelList = excelTable.ToListOf<ManageWdmTrailModel>();

            // och, oms, osc, ots: no tiene OCHn y OTUn
            var tasks = new List<Task>
            {
                Task.Run(() => ProcessBaseRecords(excelModelList, model => model.Level.Contains("OTU"), LevelType.OTU, path)),
                Task.Run(() => ProcessBaseRecords(excelModelList, model => model.Level.Contains("ODU"), LevelType.ODU, path)),
                Task.Run(() => ProcessBaseRecords(excelModelList, model => model.Level.Contains("Client"), LevelType.CLIENT, path)),
                Task.Run(() => ProcessBaseRecords(excelModelList, model => model.Level.Contains("OTS"), LevelType.OTS, path)),
                Task.Run(() => ProcessBaseRecords(excelModelList, model => model.Level.Contains("OMS"), LevelType.OMS, path)),
                Task.Run(() => ProcessBaseRecords(excelModelList, model => model.Level.Contains("OCh"), LevelType.OCH, path)),
                Task.Run(() => ProcessBaseRecords(excelModelList, model => model.Level.Contains("OSC"), LevelType.OSC, path))
            };

            await Task.WhenAll(tasks);

            var response = new DocumentUploadResponse()
            {
                RowsInserted = 0,
                FileName = Path.GetFileName(path)
            };
            return response;
        }

        private void ProcessBaseRecords(List<ManageWdmTrailModel> excelModelList, Func<ManageWdmTrailModel, bool> criteria, LevelType level, string filePath)
        {
            var excelBaseTable = excelModelList.Where(criteria).ToList();
            var tableName = $"excel_managewdmtrail_{level.ToFriendlyString()}";
            InsertDatabase(tableName, filePath, tableName, excelBaseTable);
            ProcessLevelRecords(excelBaseTable, level, filePath);
        }

        private List<ManageWdmTrailModelResult> ParseSourceSink(List<ManageWdmTrailModel> excelModelList, LevelType level)
        {
            return excelModelList
                .Aggregate(
                    new List<ManageWdmTrailModelResult>(),
                    (acc, item) =>
                    {
                        var model = new ManageWdmTrailModelResult(level)
                        {
                            Name = item.Name,
                            Source = _manageWdmTrailParser.Parse(item.Source),
                            Sink = _manageWdmTrailParser.Parse(item.Sink)
                        };
                        acc.Add(model);
                        return acc;
                    });
        }

        private void ProcessLevelRecords(List<ManageWdmTrailModel> excelLevelRecords, LevelType level, string filePath)
        {
            var excelLevelTable = ParseSourceSink(excelLevelRecords, level);
            InsertDatabase($"excel_managewdmtrail_{level.ToFriendlyString()}_source_sink", filePath, $"managewdmtrail_{level.ToFriendlyString()}_source_sink", excelLevelTable);
        }
    }
}