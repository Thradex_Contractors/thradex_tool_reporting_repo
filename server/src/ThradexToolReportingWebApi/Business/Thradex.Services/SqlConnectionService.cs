﻿using Thradex.DataAccess.Database.Interfaces;
using Thradex.Services.Interfaces;

namespace Thradex.Services
{
    public class SqlConnectionService : ISqlConnectionService
    {
        private ISqlConnectionDatabase _sqlConnectionTest;

        public SqlConnectionService(ISqlConnectionDatabase sqlConnectionTest)
        {
            _sqlConnectionTest = sqlConnectionTest;
        }

        public string Ping()
        {
            return _sqlConnectionTest.Ping();
        }
    }
}