﻿using ClosedXML.Excel;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Thradex.Models.Base;

namespace Thradex.Services.Extensions
{
    public static class ClosedXMLExcelExtensions
    {
        public static TModel ToModel<TModel>(this IXLTableRow tableRow, IEnumerable<string> headers) where TModel : class, new()
        {
            BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public;

            var publicProperties = typeof(TModel).GetProperties(bindingFlags).ToDictionary(propertyInfo => propertyInfo.Name);

            var instanceModel = new TModel();

            foreach (var head in headers)
            {
                var cellValue = tableRow.Field(head).Value;
                var propertyName = GetClearProperty(head);
                SetPropertyValue(instanceModel, propertyName, cellValue, publicProperties);
            }
            return instanceModel;
        }

        public static List<TModel> ToListOf<TModel>(this IXLTable table) where TModel : BaseModel, new()
        {
            var excelModelList = new List<TModel>();

            var headers = table.HeadersRow().CellsUsed().Select(h => h.Value.ToString()).ToList();

            //Loop over excelTable
            foreach (var excelRow in table.DataRange.Rows())
            {
                var model = excelRow.ToModel<TModel>(headers);
                excelModelList.Add(model);
            }
            return excelModelList;
        }

        private static void SetPropertyValue(object instance, string propertyName, object value, Dictionary<string, PropertyInfo> publicProperties)
        {
            PropertyInfo propertyInfo;
            publicProperties.TryGetValue(propertyName, out propertyInfo);

            if (propertyInfo != null)
            {
                propertyInfo.SetValue(instance, value, null);
            }
        }

        private static string GetClearProperty(string head)
        {
            // Characters to strip: ()\/-#
            // Characters to replace: ó
            var stripExpression = new Regex(@"[\(\)\\/\-#]");
            var sanitized = Regex.Replace(stripExpression.Replace(head, ""), @"\s+", "");
            return sanitized.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u");
        }
    }
}