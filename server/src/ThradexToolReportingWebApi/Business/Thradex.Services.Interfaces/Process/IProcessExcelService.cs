﻿using System.Threading.Tasks;
using Thradex.Models;

namespace Thradex.Services.Interfaces.ProcessExcel
{
    public interface IProcessExcelService
    {
        Task<DocumentUploadResponse> Process(string fileName);

        string FileIdentifier { get; }
    }
}