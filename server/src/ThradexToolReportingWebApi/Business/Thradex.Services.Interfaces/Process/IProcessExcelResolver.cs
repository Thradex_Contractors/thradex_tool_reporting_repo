﻿namespace Thradex.Services.Interfaces.ProcessExcel
{
    public interface IProcessExcelResolver
    {
        IProcessExcelService Resolve(string fileIdentifier);
    }
}