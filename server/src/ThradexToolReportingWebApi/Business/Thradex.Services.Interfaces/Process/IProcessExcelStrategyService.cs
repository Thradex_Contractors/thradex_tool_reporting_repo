﻿using System.Threading.Tasks;

namespace Thradex.Services.Interfaces.ProcessExcel
{
    public interface IProcessExcelStrategyService
    {
        Task Execute(string fileIdentifier, string fileName);
    }
}