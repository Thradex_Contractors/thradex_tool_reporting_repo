﻿namespace Thradex.Services.Interfaces
{
    public interface ISqlConnectionService
    {
        string Ping();
    }
}