﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thradex.Framework.Interfaces;
using Thradex.Models.Excels;

namespace Thradex.Framework
{
    public class SingleRouteSpecificsParser : IGeneralParser<WdmSingleRouteModelResult>
    {
        private readonly IGeneralParser<SourceSinkContextResult> _manageWdmTrailParser;
        private readonly IGeneralParser<WdmSingleRouteOchOduModelResult> _parserSingleRouteOchOduMapping;

        public SingleRouteSpecificsParser(IGeneralParser<SourceSinkContextResult> manageWdmTrailParser, IGeneralParser<WdmSingleRouteOchOduModelResult> parserSingleRouteOchOduMapping)
        {
            this._manageWdmTrailParser = manageWdmTrailParser;
            this._parserSingleRouteOchOduMapping = parserSingleRouteOchOduMapping;
        }

        public WdmSingleRouteModelResult Parse(string context)
        {
            var source = context;

            var positiveNegativeValues = source.Split(new string[] { "Positive:", "Negative:" }, StringSplitOptions.RemoveEmptyEntries);

            var model = new WdmSingleRouteModelResult
            {

                //Positive
                WorkingProtectionPositive = ParseSingleRouteDetails(positiveNegativeValues.First(), true)
            };

            //Negative
            if (positiveNegativeValues.Length > 1)
            {
                model.WorkingProtectionNegative = ParseSingleRouteDetails(positiveNegativeValues.Last(), false);
            }

            return model;
        }

        private List<WdmSingleRouteDetailModelResult> ParseSingleRouteDetails(string context, bool isPositive)
        {
            var trails = context.Split(new string[] { "->", "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries);
            return trails.Aggregate(
                new List<WdmSingleRouteDetailModelResult>(),
                (acc, item) =>
                {
                    var source = item.Trim();
                    if (string.IsNullOrEmpty(source)) return acc;

                    var singleRouteDetailModelResult = new WdmSingleRouteDetailModelResult
                    {
                        WdmTrailModel = _manageWdmTrailParser.Parse(source),
                        OchOduMapping = _parserSingleRouteOchOduMapping.Parse(source),
                        IsPositive = isPositive,
                    };
                    acc.Add(singleRouteDetailModelResult);
                    return acc;
                });
        }
    }
}