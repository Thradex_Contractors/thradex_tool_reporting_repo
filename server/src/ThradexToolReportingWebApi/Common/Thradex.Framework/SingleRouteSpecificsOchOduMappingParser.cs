﻿using System;
using Thradex.Framework.Interfaces;
using Thradex.Models.Excels;

namespace Thradex.Framework
{
    public class SingleRouteSpecificsOchOduMappingParser : IGeneralParser<WdmSingleRouteOchOduModelResult>
    {
        private string key = "OCh";

        public WdmSingleRouteOchOduModelResult Parse(string context)
        {
            var source = context;

            var sourceSplit = source.Split(new char[] { '-' });
            var numberOfNodes = sourceSplit.Length;

            var model = new WdmSingleRouteOchOduModelResult();
            var positionOch = Array.FindIndex(sourceSplit, s => s.Contains(key));
            if (positionOch != -1)
            {
                model.OpticalChannel = sourceSplit[positionOch];
                model.OduHo = numberOfNodes > positionOch + 1 ? sourceSplit[positionOch + 1] : string.Empty;
                model.OduLo = numberOfNodes > positionOch + 2 ? sourceSplit[positionOch + 2] : string.Empty;
            }

            return model;
        }
    }
}