﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Thradex.Framework.Interfaces;
using Thradex.Models.Excels;

namespace Thradex.Framework
{
    public class ManageWdmTrailParser : IGeneralParser<SourceSinkContextResult>
    {
        private string key = "shelf";
        private Func<string, int> extractNumber = (input) => int.Parse(Regex.Match(input, @"\d+").Value);

        public SourceSinkContextResult Parse(string context)
        {
            var source = context;

            var sourceSplit = source.Split(new char[] { '-' });

            var neName = string.Empty;

            var model = new SourceSinkContextResult
            {
                Context = context
            };
            if (source.Contains(key))
            {
                var posKey1 = sourceSplit[1].StartsWith(key);
                var posKey2 = sourceSplit[2].StartsWith(key);
                var posKey3 = sourceSplit[3].StartsWith(key);
                var posKey4 = sourceSplit[4].StartsWith(key);

                if (posKey1)
                {
                    neName = sourceSplit[0];
                }
                else if (posKey2 || posKey3)
                {
                    var f = sourceSplit[0];
                    var h = posKey3 ? $"{sourceSplit[1]}-{sourceSplit[2]}" : sourceSplit[1];
                    var j = $"{f}-{h}";
                    if (char.IsDigit(h.ToCharArray().First()))
                    {
                        neName = j;
                    }
                    else
                    {
                        neName = h;
                    }
                }
                else if (posKey4)
                {
                    var f = sourceSplit[0];
                    var h = $"{sourceSplit[1]}-{sourceSplit[2]}-{sourceSplit[3]}";
                    var j = $"{f}-{h}";
                    if (char.IsDigit(h.ToCharArray().First()))
                    {
                        neName = j;
                    }
                    else
                    {
                        neName = h;
                    }
                }
                var positionShelf = Array.FindIndex(sourceSplit, s => s.Contains(key));

                var shelfId = extractNumber(sourceSplit[positionShelf]);
                var slotId = int.Parse(sourceSplit[positionShelf + 1]);
                var board = sourceSplit[positionShelf + 2];
                var portId = extractNumber(sourceSplit[positionShelf + 3]);

                model.NeName = neName;
                model.ShelfId = shelfId;
                model.SlotId = slotId;
                model.Board = board;
                model.PortId = portId;
                model.OCHn = parseOCHn(string.Join("-", sourceSplit.Skip(positionShelf + 3)));
                model.OTUn = parseOTUn(string.Join("-", sourceSplit.Skip(positionShelf + 3)));
            }
            else
            {
                model.NeName = sourceSplit[0];
            }

            return model;
        }

        private string SanitizeSource(string context)
        {
            var regex = new Regex(@"\(([^\(\)]+)\)");
            var sourceSanitized = context;
            while (sourceSanitized.Contains("(") && sourceSanitized.Contains(")"))
            {
                sourceSanitized = regex.Replace(sourceSanitized, "UNKOWN");
            }
            return sourceSanitized;
        }

        private string[] getSanitizedChunks(string context)
        {
            var chunksSanitized = SanitizeSource(context);
            return chunksSanitized.Split(new string[] { "UNKOWN" }, StringSplitOptions.RemoveEmptyEntries);
        }

        private string parseOCHn(string context)
        {
            var chunksSanitized = getSanitizedChunks(context);
            if (chunksSanitized.Length > 1)
                return chunksSanitized[1].Length> 5 ? chunksSanitized[1].Substring(1, 5) : chunksSanitized[1].Substring(1);
            return string.Empty;
        }

        private string parseOTUn(string context)
        {
            var ochn = parseOCHn(context);
            if (string.IsNullOrEmpty(ochn)) return string.Empty;
            
            var indexOchn = context.IndexOf(ochn);
            var items = context.Substring(indexOchn).Split(new char[] { '-' });


            return items.Length > 1 ? string.Join("-", items.Skip(1)) : string.Empty;
        }
    }
}