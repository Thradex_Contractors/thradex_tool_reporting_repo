﻿using Thradex.SettingsManager.Interfaces;

namespace Thradex.SettingsManager
{
    public class GeneralSettings : IGeneralSettings
    {
        public string[] ValidExtensions => new string[] { ".xls", ".xlsx" };

        public string TargetFolderPath => @"D:\Danny\Github\thradex_tool_reporting_repo\server\src\ThradexToolReportingWebApi\ThradexToolReportingWebApi\App_Data\uploads";
    }
}