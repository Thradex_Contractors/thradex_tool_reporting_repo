﻿using System.Configuration;
using Thradex.SettingsManager.Interfaces;

namespace Thradex.SettingsManager
{
    public class MySqlDatabaseSettings : IDatabaseSettings
    {
        public string ConnectionString => ConfigurationManager.ConnectionStrings["Thradex_Mysql"].ConnectionString;
    }
}