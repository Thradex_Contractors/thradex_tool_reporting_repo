﻿namespace Thradex.Framework.Interfaces
{
    public interface IGeneralParser<TModel>
    {
        TModel Parse(string context);
    }
}