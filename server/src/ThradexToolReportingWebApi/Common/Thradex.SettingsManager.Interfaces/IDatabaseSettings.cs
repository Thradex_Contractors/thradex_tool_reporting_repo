﻿namespace Thradex.SettingsManager.Interfaces
{
    public interface IDatabaseSettings
    {
        string ConnectionString { get; }
    }
}