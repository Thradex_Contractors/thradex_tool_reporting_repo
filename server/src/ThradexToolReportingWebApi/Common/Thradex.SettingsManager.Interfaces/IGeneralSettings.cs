﻿namespace Thradex.SettingsManager.Interfaces
{
    public interface IGeneralSettings
    {
        string[] ValidExtensions { get; }
        string TargetFolderPath { get; }
    }
}