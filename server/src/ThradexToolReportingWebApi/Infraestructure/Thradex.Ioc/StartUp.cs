﻿using SimpleInjector;
using SimpleInjector.Lifestyles;
using Thradex.DataAccess.Database;
using Thradex.DataAccess.Database.Interfaces;
using Thradex.Framework;
using Thradex.Framework.Interfaces;
using Thradex.Models.Excels;
using Thradex.Services;
using Thradex.Services.Interfaces;
using Thradex.Services.Interfaces.ProcessExcel;
using Thradex.Services.ProcessExcel;
using Thradex.SettingsManager;
using Thradex.SettingsManager.Interfaces;

namespace Thradex.Ioc
{
    public static class StartUp
    {
        public static Container container;

        public static void RegisterAllComponents()
        {
            container = new Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            RegisterSettingsComponents();
            RegisterDatabaseComponents();
            RegisterServicesComponents();
            container.Verify();
        }

        private static void RegisterSettingsComponents()
        {
            container.Register<IDatabaseSettings, MySqlDatabaseSettings>();
            container.Register<IGeneralSettings, GeneralSettings>();
        }

        private static void RegisterDatabaseComponents()
        {
            container.Register<IExcelToDatabase, ManageWdmTrailDatabase>();
            container.Register<ISqlConnectionDatabase, MySqlConnectionDatabase>();
        }

        private static void RegisterServicesComponents()
        {
            container.Register<IProcessExcelStrategyService, ProcessExcelStrategyService>();
            container.Register<IProcessExcelResolver, ProcessExcelResolver>((Lifestyle.Singleton));
            container.RegisterCollection(typeof(IProcessExcelService), new[] { typeof(ExcelManageWdmTrailService), typeof(ExcelSingleRouteSpecificsService) });

            container.Register<ISqlConnectionService, SqlConnectionService>();
            container.Register<IGeneralParser<SourceSinkContextResult>, ManageWdmTrailParser>();
            container.Register<IGeneralParser<WdmSingleRouteModelResult>, SingleRouteSpecificsParser>();
            container.Register<IGeneralParser<WdmSingleRouteOchOduModelResult>, SingleRouteSpecificsOchOduMappingParser>();
        }
    }
}