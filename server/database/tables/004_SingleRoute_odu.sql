CREATE TABLE `excel_singleroute_odu` (
    `SingleRouteOduId` INT NOT NULL AUTO_INCREMENT,
	`RowIndex` INT NOT null,
	`Name` VARCHAR(255) NULL,
	`Level` VARCHAR(255) NULL,
	`Direction` VARCHAR(255) NULL,
	`Source` VARCHAR(5000) NULL,
	`SourceWavelength` VARCHAR(255) NULL,
	`Sink` VARCHAR(5000) NULL,
	`SinkWavelength` VARCHAR(255) NULL,
	`WorkingRoute` VARCHAR(5000) NULL,
	`ProtectionRoute` VARCHAR(5000) NULL,
	`BearerRateMbits` VARCHAR(255) NULL,
	`Rate` VARCHAR(255) NULL,
	`Customer` VARCHAR(255) NULL,
    PRIMARY KEY (`SingleRouteOduId`)
);