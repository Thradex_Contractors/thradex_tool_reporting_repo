CREATE TABLE `excel_nereport` (
    `NEReportId` INT NOT NULL AUTO_INCREMENT,
    `Name` VARCHAR(255) NULL,
    `Type` VARCHAR(255) NULL,
    `IPAddress` VARCHAR(255) NULL,
    `MACAddress` VARCHAR(255) NULL,
    `ID` VARCHAR(255) NULL,
    `SoftwareVersion` VARCHAR(255) NULL,
    `PhysicalLocation` VARCHAR(255) NULL,
    `CreateTime` VARCHAR(255) NULL,
    `FiberCableCount` VARCHAR(255) NULL,
    `RunningStatus` VARCHAR(255) NULL,
    `Subnet` VARCHAR(255) NULL,
    `SubnetPath` VARCHAR(255) NULL,
    `Alias` VARCHAR(255) NULL,
    `Remarks` VARCHAR(255) NULL,
    `PatchVersionList` VARCHAR(255) NULL,
    `CustomizedColumn` VARCHAR(255) NULL,
    `LSRID` VARCHAR(255) NULL,
    `MaintenanceStatus` VARCHAR(255) NULL,
    `GatewayType` VARCHAR(255) NULL,
    `Gateway` VARCHAR(255) NULL,
    `Optical` VARCHAR(255) NULL,
    `SubrackType` VARCHAR(255) NULL,
    `ConferenceCall` VARCHAR(255) NULL,
    `OrderwirePhone` VARCHAR(255) NULL,
    `Subtype` VARCHAR(255) NULL,
    PRIMARY KEY (`NEReportId`)
);