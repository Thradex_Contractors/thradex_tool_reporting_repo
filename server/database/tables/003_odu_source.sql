CREATE TABLE `excel_odu_source` (
    `OduSourceId` INT NOT NULL AUTO_INCREMENT,
    `RowIndex` INT NOT null,
    `Name` VARCHAR(255) NULL,
    `NeName` VARCHAR(255) NULL,
    `ShelfId` INT NULL,
    `SlotId` INT NULL,
    `Board` VARCHAR(255) NULL,
    `PortId` INT NULL,
    PRIMARY KEY (`OduSourceId`)
);