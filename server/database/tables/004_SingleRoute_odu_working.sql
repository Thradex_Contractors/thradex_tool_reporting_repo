CREATE TABLE `excel_singleroute_odu_working` (
    `SingleRouteOduWorkingId` INT NOT NULL AUTO_INCREMENT,
    `RowIndex` INT NOT null,
    `Name` VARCHAR(255) NULL,
	`IsPositive` BOOLEAN NOT NULL,
    `NeName` VARCHAR(255) NULL,
    `ShelfId` INT NULL,
    `SlotId` INT NULL,
    `Board` VARCHAR(255) NULL,
    `PortId` INT NULL,
    `OpticalChannel` VARCHAR(255) NULL,
    `OduHo` VARCHAR(255) NULL,
    `OduLo` VARCHAR(255) NULL,
    PRIMARY KEY (`SingleRouteOduWorkingId`)
);
